# Repository overview

This is the repository for all Privacy1 public documentations. All doc are written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format.

Privacy1 proprietary documentations are avaialble [here](http://app.privacyone.co/documentation)

---

    Copyright (c) 2021 Privacy1 AB

    THIS REPOSITORY IS THE PROPERTY OF PRIVACY1 AB

