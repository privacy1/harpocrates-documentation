# harpocrates-ecs-deployment
This is the repository to describe how to deploy Privacy1 data protection system in Amazon Elistic Container Service

## Create ECS cluster with EC2 type
To support multiple ENIs, ENI trunking is used. Please choose EC2 type 
that supports ENI trunking [list](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/container-instance-eni.html)

You may choose to use your existing cluster otherwise follow the [link](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/create_cluster.html)
and choose EC2 type Linux + Networking to create a new cluster. 

When choosing an EC2 instance type, you need to balance among pricing, memory requirments and 
ENI (Elastic Network Interface) limits. Pricing choice is regulated by your budget. Each of the 
microservice requires at least 500MB to startup. With regards to ENI limits, there are limits on
ENI associated with each EC2 instance type. Each service needs an ENI. So we need EC2 supports 4+ ENI 
to depoly Privacy1 services. 

Note: Please enable AWSVPC Trunking by going into your Account Settings page of your ECS console. 

Specify a key-pair which you may later on access your EC2 instance via ssh. If not specified, a new 
key-pair will be created.

You may use your existing VPC when creating your ECS cluster. Otherwise a new VPC will be created 
with the creation of your new ECS cluster.

Note: There will be 2 or several subnets created in your VPC. Remember which subnet your EC2 hosts

## Prepare docker access
PrivacyOne microservices are delivered as docker containers and they are hosted in PrivacyOne docker container 
registry. In order for your business to access the containers, contact PrivacyOne so we can generate a temporary 
docker login token for your business. Login to your ec2 host and run the command below.

```
docker login -u AWS -p <ONE_TIME_LOGIN_TOKEN> https://344250484034.dkr.ecr.us-east-2.amazonaws.co`
```

## Create RDS mysql database
Create a new parameter group on the dialog, select the MySQL family compatible to your MySQL database version, 
give it a name, confirm and then follow the instructions below

1. Select the just created Parameter Group and issue “Edit Parameters”.
2. Look for the parameter ‘log_bin_trust_function_creators’ and set its value to ’1′.
3. Save the changes.

Create a RDS Mysql database for Privacy1 data protection system. Use the same VPC that you chose to create or 
select in the previous step. In the field of DB parameter group, choose the parameter group you have created
above.


## Define task definition.
Configure the task definition which are defined in the taskDefs folder of this repository. Privacy1 system 
uses microservice design pattern. Hence you will find one individual JSON task definifion for a specific 
microservice. You need to create one task definition for each of the following microservices.

* Privacy Console
* Privacy Front
* Cerberus
* Keychain
* Keychain Analytics (Optional)
* DPIA (Optional)
* CookieJar (Optional)
* Gap Analysis (Optional)

### Step 1 Select launch type compatibility
Select EC2 launch type

### Step 2 Configure task and container definitions
1. In network mode, choose awsvpc
2. Scroll to the bottom of the page, Click **Config via JSON** and Copy one of the predefined task definitions to the content page
3. Save
4. For each of the microservice, we use CloudWatch for logging. As noted in each of the task definition file, we have created 
a seperate log group for its corresponding service logging. So you need to go to the CloudWatch console to create the corresponding
log group manually to match the aswlogs-group settings.

```
options": {
          "awslogs-group": "dpia-logs",

```

Go back to Step 1 and repeat until you have the needed microservices task definition configured.

## Prepare configurations for all the microservices
Service configuration files are prepared in the etc folder of this repository. It follows the structure of each microservice has its
own configuration folder underneath the privacyone parent folder

/etc/privacyone/keychain
/etc/privacyone/privacyconsole

## Create ECS service.
Create service from AWS console. Choose the existing VPC and subnets. Note, the subnets should be the
one that your EC2 instance host on. 

### Create service for cerberus, keychain
1. Choose service type of EC2.
2. service name **MUST** be the microservice DNS names `cerberus`, `keychain`, `privacyfront`, `ldar`, `dpia`, `cookiejar`, `gapanalysis`, `keychainanalytics`
3. Choose VPC and subnets that have been created. Note: subnets must be the EC2 subnet.
4. Choose security group created with VPC.
5. IMPORTANT: check the checkbox "Enable service discover integration". Create a new private namespace with the name `privacyone`. In the following steps if "privacyone"
namespace already exists, choose it.
6. Choose "Create new service discovery service". Fill service discover name as the microservice DNS name specified in step 2.

### Create service for privacy front and privacy console.
Follow the steps above of creating service for cerberus,, keychain for normal steps.
privacy front and privacy console need public ip to be access by administrator and user. Load balance need
be enalbed to assign public IP. 
1. Choose Network load balancer.
2. Creat a new load balancer in https://eu-north-1.console.aws.amazon.com/ec2/v2/home?region=eu-north-1#LoadBalancers:sort=loadBalancerName
3. Fill load balance name.
4. Choose VPC and subnets where EC2 hosted.
5. Add listener. Choose 443 for privacy console, 8443 for privacy front.
6. When adding listener, create target group for each listener. https://eu-north-1.console.aws.amazon.com/ec2/home?region=eu-north-1#TargetGroups:
7. Choose "instance" as target type.
8. Fill "target group name"
9. Choose port same with load balancer.
10. Choose VPC same with cluster.


