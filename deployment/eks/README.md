# Get Started with Harpocrates 

This tutorial is the first article of a series of documentations that explain how to deploy PRIVACY1 Harpocrates helm charts 
using Amazon EKS in AWS Marketplace environment.

There are three tutorials regarding this topic. 

[**Pre-deployment**](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/pre-deployment.md) 

This tutorial prepares a running EKS cluster for you. Feel free to skip this section if you've already had one running.

[**Deployment**](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/deployment.md)

This section describes the actual deployment of Harpocrates app using helm charts. It also consists useful information 
of how to manage a running cluster.


[**Post-deployment**](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/post-deployment.md) 

The final chapter of the trilogy. It explains how to customize Harpocrates service to your business needs. For example, 
it covers the topics regarding how to change default user, service account passwords, and how to apply changes to the cluster configuration.


