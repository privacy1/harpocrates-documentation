# Harpocrates post-deployment

> **Read time**: 15 minutes

> **Prerequisite**: Harpocrates app is running in an EKS cluster

> **Mission**: Harpocrates app is well configured for your production environment

## Associate domain names
Find aws-assigned load balancer domain for Privacy Console using the following command
```
$ kubectl get services | grep privacy-console | awk '{print $4}'
```
In your DNS server add one new DNS `CNAME` record to bind your actual Privacy Console domain e.g. console.privacy.<YOUR_BIZ_DOMAIN> 
to this aws-assigned load balancer domain.

Find aws-assigned load balancer domain for Privacy Front using the following command
```
$ kubectl get services | grep privacyfront | awk '{print $4}'
```
In your DNS server add one new DNS `CNAME` record to bind your actual Privacy Front domain e.g. privacyfront.privacy.<YOUR_BIZ_DOMAIN> 
to this aws-assigned load balancer domain.

## Configure firewall rules to whitelist your business internal access

The default firewall rule allows 0.0.0.0/0 to access Harpocrates infrastructure. You must allow **ONLY** your business user 
accounts to access Privacy Console and **ONLY** your business service accounts to access Harpocrates micro-services. 
      
1. Open the [Amazon Elastic Compute Cloud (Amazon EC2) console](https://console.aws.amazon.com/ec2/).
2. Click Load Balancers
3. Find the Privacy Console load balancer item and scroll to the Security section
4. Click Source Security Group by its ID
5. Click Actions -> Edit inbound rules
6. Restrict Harpocrates micro-service access to only your business service account by allowing your internal service IP 
   addresses or security group to access ports 8090 (Harpocrates Keychain service ), 8091 (Harpocrates LDAR service), 
   8093 (Harpocrates Cerberus service). Find more details at [Harpocrates documentation](https://app.privacyone.co/documentation).
7. Restrict Privacy Console access to your employees by allowing only your business VPN address, or office IP range to 
   access port 443 (Harpocrates Privacy Console service).

## Configure user and service accounts in Harpocrates

### Update your Privacy Console Admin account default password

1. Open your Privacy Console https://<YOUR_PRIVACY_CONSOLE_DOMAIN>.

2. Login with the default username:**admin** password:**amdin**

3. Go to `My Account` tab from the top right dropdown menu and choose to `Update your account password`
      
### Update Privacy Front service account default password

1. Open your Privacy Front https://<YOUR_PRIVACY_FRONT_DOMAIN>.

2. Login with the default username:**privacyfront** password:**privacyfront**

3. Go to `My Account` tab from the top right dropdown menu and choose to `Update your account password`

4. Update Privacy Front service password in service properties by running

```
$ kubectl edit configmap privacyfront-config
```
The above command will open `vi` editor. Update the value of field `data.privacyfront.properties.privacyone.cerberus.accountPassword`
with your new Privacy Front account password. Type `:wq` save and exit.

5. Restart Privacy Front service
```
$ kubectl scale statefulset privacyfront --replicas=0
$ kubectl scale statefulset privacyfront --replicas=1
```

### Update LDAR service account default password

1. Open your Privacy Console https://<YOUR_PRIVACY_CONSOLE_DOMAIN>.

2. Login with the default username:**ldar**. key:**ldar**

3. Go to `My Account` tab from the top right corner and choose to `Update your account password`.

4. Update LDAR service password in service properties by running
```
$ kubectl edit configmap ldar-config
```

The above command will open vi editor. Update the value of field `data.ldar.properties.privacyone.cerberus.accountPassword`
with your new ldar service account password. Type `:wq` save and exit.

5. Restart LDAR service
```
$ kubectl scale statefulset ldar --replicas=0
$ kubectl scale statefulset ldar --replicas=1
```

### Update LDAR service key

1. Open your Privacy Console https://<YOUR_PRIVACY_CONSOLE_DOMAIN>.

2. Login with Admin account.

3. Click on "Keychain". Navigate to "Services" from left side menu.

4. Click on "GENERATE SERVICE KEY" button. Write down the generated service key and keep it in a safe place.

5. Update LDAR service key in service properties by running
```
$ kubectl edit configmap ldar-config
```
        
The above command will open vi editor. Update the value of field `data.ldar.properties.privacyone.keychain.ldar.serviceKey`
with new generated key value. Type `:wq` save and exit.

6. Restart LDAR service
```
$ kubectl scale statefulset ldar --replicas=0
$ kubectl scale statefulset ldar --replicas=1
```

### Config service account (Optional)

Service account is used in LDAR and Keychain Analytics services to grant access to data stored in google storage or aws S3.
Create two service accounts by following [LDAR and Keychain Analytics user guide](https://app.privacyone.co/documentation)
and download json key files. Copy LDAR key json file to /tmp/LDAR_JSON_KEY_FILE. Copy Keychain Analytics key json file to
/tmp/KCA_JSON_KEY_FILE. Fun below commands to update service account for LDAR and Keychain Analytics services.

```
$ kubectl create configmap ldar-iam-config \
  --from-file=iam-service-account-key.json=/tmp/LDAR_JSON_KEY_FILE \
  --dry-run -o yaml | kubectl apply -f -
$ kubectl create configmap keychainanalytics-iam-config \
  --from-file=iam-service-account-key.json=/tmp/KCA_JSON_KEY_FILE \
  --dry-run -o yaml | kubectl apply -f -
```

Restart LDAR and Keychain Analytics service
```
$ kubectl scale statefulset ldar --replicas=0
$ kubectl scale statefulset ldar --replicas=1

$ kubectl scale statefulset keychain-analytics --replicas=0
$ kubectl scale statefulset keychain-analytics --replicas=1
```
