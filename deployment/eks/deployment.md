# Harpocrates Deployment

> **Read time**: 15 minutes

> **Prerequisite**: You have gone through pre-deployment and an EKS cluster is up and running

> **Mission**: You manage to deploy Harpocrates in an EKS cluster using the AWS Marketplace

## Overview
Once your have a kubernetes cluster running, it is time to install Harpocrates kubernetes app. 
In this tutorial we walk you, step by step, through the process of using the AWS Marketplace 
to deploy applications on a running EKS cluster. We provide the AWS CloudFormation to deploy the
Harpocrates with the configuration items in the UI. This document instruct you install Harpocrate paid
version on your cluster.

## Deploy with CloudFormation
After create the EKS cluster with cloudformation, you can start to deploy the Harpocrates. Before that, you need create
a serviceaccount which can call the AWS cloud API properly.

### Connect to your cluster
Connect to EKS cluster using AWS cli
```
$ aws eks update-kubeconfig --name <ClusterName> --region <Region>
$ kubectl get svc
```

### Create service account

#### Create an OpenID Connect provider
```
$ CLUSTER_NAME=<ClusterName>
$ REGION=<region>
$ eksctl utils associate-iam-oidc-provider \
    --name "${CLUSTER_NAME}" \
    --region "${REGION}" \
    --approve
```
#### Create an IAM role that has appropriate IAM policies
```
$ ISSUER_URL=$(aws eks describe-cluster \
  --name "${CLUSTER_NAME}" \
  --region "${REGION}" \
  --query cluster.identity.oidc.issuer \
  --output text)

$ ISSUER_HOSTPATH=$(echo $ISSUER_URL | cut -f 3- -d'/')

$ ACCOUNT_ID=$(aws sts get-caller-identity --query Account --output text)

$ PROVIDER_ARN="arn:aws:iam::$ACCOUNT_ID:oidc-provider/$ISSUER_HOSTPATH"

$ NAMESPACE="default"

$ SERVICE_ACCOUNT_NAME="harpocrates-serviceaccount"

$ IAM_METER_POLICY_ARN="arn:aws:iam::aws:policy/AWSMarketplaceMeteringRegisterUsage"

$ cat > registerusage-trust-policy.json << EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Effect": "Allow",
        "Principal": {
          "Federated": "$PROVIDER_ARN"
        },
        "Action": "sts:AssumeRoleWithWebIdentity",
        "Condition": {
          "StringEquals": {
            "${ISSUER_HOSTPATH}:aud": "sts.amazonaws.com",
            "${ISSUER_HOSTPATH}:sub": "system:serviceaccount:${NAMESPACE}:${SERVICE_ACCOUNT_NAME}"
          }
        }
      }
    ]
  }
  EOF

$ ROLE_NAME="registerusage-role"

$ aws iam create-role \
    --role-name "${ROLE_NAME}" \
    --assume-role-policy-document file://registerusage-trust-policy.json

$ aws iam attach-role-policy \
    --role-name "${ROLE_NAME}" \
    --policy-arn "${IAM_POLICY_ARN}"

$ ROLE_ARN=$(aws iam get-role \
    --role-name "${ROLE_NAME}" \
    --query Role.Arn --output text)
```

#### Annotate the existing Kubernetes Service Account with the IAM role
```
$ kubectl create serviceaccount "${SERVICE_ACCOUNT_NAME}"
$ kubectl annotate sa "${SERVICE_ACCOUNT_NAME}" eks.amazonaws.com/role-arn="${ROLE_ARN}"
```

### Deploy Harpocrates paid version
Go to Hapocrates paid version on AWS marketplace[Harpocrates Paid](https://aws.amazon.com/marketplace/pp/B08KH7Y6BY).
Click "Continue to Subscribe". After subscribe, you can find your subscription in "AWS marketplace subscription".

Now you can deploy Harpocrates to your cluster. We use cloudformation to automate the process.

1. From "service" menu, choose "CloudFormation".
2. Click "Create stack" menu. Choose "With new resources (standard)".
3. "Create stack" web page shows up. In "Specify template", choose "Upload a template file". Then click "Choose file" button.
4. A file choose window pops up. Choose the "deployment/aws/CloudFormation/harpocrates-paid-stack.yaml" in
   [harpocrates-document](https://gitlab.com/privacy1/harpocrates-documentation.git) repo. You can clone this repo to your local server before deployment.
5. A gui web page will comes out after you choose CloudFormation template to guide you fill the fields step by step.
6. Give a stack name in "Stack name". You can give anything. It does not impact your deployment.
7. For "EKS Kube Cluster Name". Open the "CloudFormaton->Stack". Choose stack that you create EKS cluster. In "Output" page, copy the cluster name to here.
8. Choose "Harpocrates application version" 1.1.0.
9. Give a password for "Mysql Master User Password".
10. Give the service account name created above.
11. You need TLS certificate and key to be used by Harpocrates. Since file can not be upload and CloudFormation has limitation
    of size of input string. You need split the TLS certificate and key to several parts. For parameter "How many parts of encrypted TLS private key" run
    below command to get the number and fill in field.
    ```
    cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\1\ /g' | wc -w
    ```
12. For "The first part of encrypted TLS private key for connecting to domain", run below command, copy the output to file the field
    ```
    cat YOUR_TLS_KEY_FILE.key | base64 -w 0 | sed -r 's/(.{4096})/\1\ /g' | awk '{ print $1 }
    ```
13. If your tls key file has 2 or 3 part, run above command again only change the awk print to $2 or $3.
14. For "How many parts of encrypted TLS certificate" parameter, run below command, copy the output to field.
    ```
    cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\1\ /g' | wc -w
    ```
15. For "The first part of encrypted TLS certificate for connecting to domain" parameter, run below command, copy the output to the field.
    ```
    cat YOUR_TLS_CRT_FILE.crt | base64 -w 0 | sed -r 's/(.{4096})/\1\ /g' | awk '{ print $1 }'
    ```
16. If your tls certificate file has 2 or 3 part, run above command again only change the awk print to $2 or $3.
17. Give the domain name of PrivacyConsole. The domain name should consistent with your TLS certificate CN.
18. Give the domain name of PrivacyFront. The domain name should consistent with your TLS certificate CN.
19. From the menu list to choose the subnet that has "Private Subnet 1".
20. From the menu list to choose the subnet that has "Private subnet 2".
21. From the menu list to choose VPC ID that you create with EKS cluster CloudFormation.
22. For "Node SecurityGroup ID", Open the "CloudFormaton->Stack". Choose stack that you create EKS cluster. In "Output" page, copy the node securitygropu Id to this field.
23. After all above config parameter filled, click "Next" button.
24. In "Permissions" section. choose the role you have create in pre-deployment [Create a role to create cloudformation stack](https://gitlab.com/privacy1/harpocrates-documentation/-/blob/master/deployment/aws/pre-deployment.md#create-a-role-to-create-cloudformation-stack).
25. Click "Next" and then "Create stack".


### Manage MySql database
After you run the harpocrates-paid-stack.yaml above, a RDS Mysql instance is created. Choose "Service" -> "RDS",
you can find your DB instance. The Mysql database store important data used by Harpocrates. You can use RDS to manage
backup.


### Manage EKS cluster
After deploy the harpocrates, you can use kubectl to connect to your EKS cluster to check the cluster resource.

#### Configure aws client authentication
Configure your aws client authentication with the same account deploying the harpocrates.
```
$ aws configure
```
Follow the screen instruction to input your AWS Access Key ID, AWS Secret Access Key and region.

#### Connect to EKS cluster.
```
$ aws eks update-kubeconfig --name <Your_cluster_name> --region <Your_region>
```

#### Check cluster resources.
```
$ kubectrl get pods
$ kubectrl get svc
```

### Delete the Harpocrates
If you want to delete your Harpocrates deployment, you can delete the CloudFormation stack. It will help you
to delete all the resources created by the CloudFormation.

> NOTE: Make sure you have backed up all
> * Harpocrates database
> * Harpocrates secrets
> * Harpocrate service keys

Choose "Services"->"Cloudformation". From the stack list, find the stack that deploy the Harpocrates.
Click "Delete".

