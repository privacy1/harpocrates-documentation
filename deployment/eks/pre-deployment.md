# Harpocrates pre-deployment

> **Read time**: 15 minutes

> **Prerequisite**: You have an active [AWS account](https://aws.amazon.com/)

> **Mission**: You have a running EKS cluster

## Install eksctl tool
Follow the instructions [here](https://github.com/weaveworks/eksctl) to install eksctl, a simple CLI tool for creating clusters on EKS.

## Install AWS CLI

You should have awscli installed. Check awscli version by issuing
```
$ aws --version
```

If not, follow the instruction below.

### Linux
Please follow AWS CLI [install instructions](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html)
to install the AWS command line tool. A quick cheatsheet is listed here.

```
$ pip3 install awscli --upgrade --user
```

Update installed awscli to the latest version
```
$ pip3 install --upgrade --user awscli
```

### Mac
Please follow AWS CLI [Mac install instructions](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-mac.html)
to install the AWS command line tool. A quick cheatsheet is listed here.

```
$ curl "https://awscli.amazonaws.com/AWSCLIV2.pkg" -o "AWSCLIV2.pkg"
$ sudo installer -pkg AWSCLIV2.pkg -target /
```


## Create EKS required policies

Running cloudformation to create EKS and harpocrates requires a number of AWS permissions in place. Navigate to IAM Management Console-> IAM -> Policies.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/aws+marketplace/iam+policy.png" alt="IAM policy" width="400"/>

Click `Create policy` button and Create a policy with properties defined in the Appendix section.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/aws+marketplace/create+json+iam+policy.png" alt="Create plicy" width="700"/>


## Configure EKS required policies

### Existing user
If you'vd already had an existing aws user, you can attach the policy created above to the existing user and skip the 
`New user` section.

1. Open the [IAM console](https://console.aws.amazon.com/iam/)
2. Click Users tab -> the user you are planning to attach the policy we created above
3. Click Add permissions -> Attach existing policies directly and choose the policy you created from the last step
4. Click Review -> Add permissions

### New user
1. Open the [IAM console](https://console.aws.amazon.com/iam/)
2. Click Add user -> Enter User name and check Programmatic access box -> Click Permissions button
3. Click Attach existing policies directly and choose the policy you created from the last step
4. Continue and finish all the necessary steps for creating your user.
5. At the last step you will be prompted an Access key ID and Secret Access key. Save them on your computer safely and 
   will be using the two piece of information in the next step.
6. Run the following command `$ aws configure` and enter the Access key ID and Secret Access key. Also enter the target region.

## Way of deploy Harpocrates
Two kinds of method can be used to deploy the Harpocrates.

### Deploy with Helm
This method is suitable for that you have a existing EKS cluster or you can create a new EKS cluster. You can manually create a RDS
MySql database. You can use helm to deploy the Harpocrates.

### Deploy with CloudFormation
We recommend to use CloudFormation to deploy the Harpocrates. CloudFormation will create EKS cluster and RDS database with specific
VPC network, and deploy the Harpocrates on EKS cluster. This method provide the best security

## Create a cluster with CloudFormation

### Create a role to create cloudformation stack.
Two cloudformation stacks are needed to be created to deploy Harpocrates. One is to create EKS cluster and VPC. Another is to create
RDS and call helm chart. To make it is easy for your account to have the proper access permission for all necessary operation, you can
create a role for cloudformation to use for all operation.

You can create a role with AdministratorAccess policy

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
```
Edit the Trust Relationship of this role as
```
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudformation.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
```
### Create EKS cluster stack

Clone the repository "https://gitlab.com/privacy1/harpocrates-documentation" to your local directory.
1. Login to AWS cloud console with your account.
2. Choose CloudFormation from "Service" menu.
3. Click "Create stack", choose "With new resources(standard)"
4. In "Template is ready", choose "Upload a template file". Click "Choose file". Choose from the harpocrates-documentation repository at directory "/harpocrates-documentation/deployment/aws/CloudFormation/eks-cluster-stack.yaml". Click "Next".
5. Enter a name for "Stack name".
6. For "Availability Zones", choose all items in menu list.
7. Add IP address that need to access your EKS cluster. You can make the default value to be used for public access.
8. Choose a SSH key from the menu list. If you do not have a key, please create one. This key is used to login to EKS node. But usually you do not need it.
9. Remain the "Config set name" empty.
10. "Per-account shared resources", Choose "No" if you already deployed another EKS Quick Start stack in your AWS account.
11. "Per-Region shared resources", Choose "No" if you already deployed another EKS Quick Start stack in your Region.
12. You can keep all configuration items in "VPC network configuration" as default value.
13. Give a name for "EKS cluster name".
14. You can keep all configuration items in "Default EKS node group configuration" as default.
15. Update "Quick Start S3 bucket Region" to your deploy region.
16. Click "Next" to the next step.
17. In "Permission" section, Choose "IAM role name" and choose the role from menu list which is created in above step.
18. Click "Stack creation options", choose "Disable" on "Rollback on failure" for debug in case of failure.
19. Click "Next" and check all information. Check the acknowledge checkbox.
20. Click "Create stack" to start deployment.

After cloudformation deployment finish, A EKS cluster is created.

## Create a cluster with CLI
In this step we use a command line tool eksctl to create a new cluster for accommodating Harpocrates app which will be 
deployed in the next [tutorial](https://gitlab.com/privacy1/helm-chart-repo/-/blob/master/doc/deployment.md).

```
$ eksctl create cluster --name harpocrates --region <YOUR_TARGET_REGION> --nodegroup-name harpocrates-nodegroup --node-type t3.medium --nodes 3
```

## Delete a cluster
In case you want to delete a cluster, here is the command for you to run with caution. 

> NOTE: Make sure you have backed up all 
> * Harpocrates database 
> * Harpocrates secrets
> * Harpocrate service keys

```
$ eksctl delete cluster --name harpocrates --region <YOUR_TARGET_REGION>
```

## Appendix

### IAM policy

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "ec2:DeleteInternetGateway",
            "Resource": "arn:aws:ec2:::internet-gateway/"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "acm:GetCertificate",
                "acm:ImportCertificate",
                "acm:ListTagsForCertificate",
                "acm:DescribeCertificate",
                "acm:ListCertificates",
                "acm:ExportCertificate",
                "autoscaling:DeleteAutoScalingGroup",
                "autoscaling:DescribeScalingActivities",
                "autoscaling:CreateLaunchConfiguration",
                "autoscaling:DescribeAutoScalingGroups",
                "autoscaling:CreateAutoScalingGroup",
                "autoscaling:UpdateAutoScalingGroup",
                "autoscaling:DeleteLaunchConfiguration",
                "autoscaling:DescribeLaunchConfigurations",
                "aws-marketplace:ListBuilds",
                "aws-marketplace:UpdateAgreementApprovalRequest",
                "aws-marketplace:ListAgreementRequests",
                "aws-marketplace:AcceptAgreementApprovalRequest",
                "aws-marketplace:GetAgreementTerms",
                "aws-marketplace:Unsubscribe",
                "aws-marketplace:GetAgreementApprovalRequest",
                "aws-marketplace:RejectAgreementApprovalRequest",
                "aws-marketplace:ViewSubscriptions",
                "aws-marketplace:SearchAgreements",
                "aws-marketplace:DescribeAgreement",
                "aws-marketplace:ListAgreementApprovalRequests",
                "aws-marketplace:GetAgreementRequest",
                "aws-marketplace:DescribeBuilds",
                "aws-marketplace:StartBuild",
                "aws-marketplace:Subscribe",
                "aws-portal:*",
                "cloudformation:*",
                "ec2:CreateDhcpOptions",
                "ec2:AuthorizeSecurityGroupIngress",
                "ec2:AttachInternetGateway",
                "ec2:DeleteRouteTable",
                "ec2:RevokeSecurityGroupEgress",
                "ec2:CreateRoute",
                "ec2:CreateInternetGateway",
                "ec2:DeleteInternetGateway",
                "ec2:DescribeKeyPairs",
                "ec2:ImportKeyPair",
                "ec2:CreateTags",
                "ec2:RunInstances",
                "ec2:DisassociateRouteTable",
                "ec2:RevokeSecurityGroupIngress",
                "ec2:DescribeImageAttribute",
                "ec2:DeleteDhcpOptions",
                "ec2:DeleteNatGateway",
                "ec2:CreateSubnet",
                "ec2:DescribeSubnets",
                "ec2:CreateNatGateway",
                "ec2:CreateVpc",
                "ec2:DescribeVpcAttribute",
                "ec2:ModifySubnetAttribute",
                "ec2:DescribeAvailabilityZones",
                "ec2:ReleaseAddress",
                "ec2:AssociateDhcpOptions",
                "ec2:DeleteLaunchTemplate",
                "ec2:DescribeSecurityGroups",
                "ec2:CreateLaunchTemplate",
                "ec2:DescribeVpcs",
                "ec2:DeleteSubnet",
                "ec2:AssociateRouteTable",
                "ec2:DescribeInternetGateways",
                "ec2:DescribeAccountAttributes",
                "ec2:DescribeRouteTables",
                "ec2:DescribeLaunchTemplates",
                "ec2:CreateRouteTable",
                "ec2:DetachInternetGateway",
                "ec2:DeleteVpc",
                "ec2:DescribeAddresses",
                "ec2:DeleteTags",
                "ec2:DescribeDhcpOptions",
                "ec2:CreateSecurityGroup",
                "ec2:ModifyVpcAttribute",
                "ec2:AuthorizeSecurityGroupEgress",
                "ec2:DescribeTags",
                "ec2:DeleteRoute",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:DescribeNatGateways",
                "ec2:AllocateAddress",
                "ec2:DescribeImages",
                "ec2:DeleteSecurityGroup",
                "ecr:GetAuthorizationToken",
                "eks:DescribeFargateProfile",
                "eks:UpdateNodegroupConfig",
                "eks:ListClusters",
                "eks:CreateCluster",
                "eks:UntagResource",
                "eks:TagResource",
                "eks:ListTagsForResource",
                "eks:UpdateClusterConfig",
                "eks:DescribeNodegroup",
                "eks:ListNodegroups",
                "eks:DeleteCluster",
                "eks:DeleteNodegroup",
                "eks:DescribeCluster",
                "eks:UpdateClusterVersion",
                "eks:UpdateNodegroupVersion",
                "eks:ListUpdates",
                "eks:CreateNodegroup",
                "eks:ListFargateProfiles",
                "eks:DescribeUpdate",
                "iam:PutRolePolicy",
                "iam:AddRoleToInstanceProfile",
                "iam:ListRolePolicies",
                "iam:DeleteOpenIDConnectProvider",
                "iam:ListPolicies",
                "iam:GetRole",
                "iam:DeleteRole",
                "iam:GetOpenIDConnectProvider",
                "iam:GetRolePolicy",
                "iam:CreateInstanceProfile",
                "iam:ListInstanceProfilesForRole",
                "iam:PassRole",
                "iam:DeleteRolePolicy",
                "iam:DeleteInstanceProfile",
                "iam:ListRoles",
                "iam:CreateServiceLinkedRole",
                "iam:ListAccountAliases",
                "iam:ListGroups",
                "iam:RemoveRoleFromInstanceProfile",
                "iam:CreateRole",
                "iam:AttachRolePolicy",
                "iam:DetachRolePolicy",
                "iam:ListAttachedRolePolicies",
                "iam:CreateAccountAlias",
                "iam:DeleteServiceLinkedRole",
                "iam:CreateUser",
                "iam:GetInstanceProfile",
                "iam:ListInstanceProfiles",
                "iam:CreateOpenIDConnectProvider",
                "iam:ListUsers",
                "rds:*",
                "s3:*",
                "sns:ListTopics",
                "sns:GetTopicAttributes",
                "ssm:GetParameter",
                "ssm:DescribeParameters",
                "ssm:GetParameters",
                "ssm:PutParameter",
                "ssm:AddTagsToResource"
            ],
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": "ssm:GetParameter",
            "Resource": "arn:aws:ssm:::parameter/"
        }
    ]
}
```
