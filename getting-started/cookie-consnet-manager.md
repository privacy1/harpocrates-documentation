# Cookie Consent Manger User Guide

> **Read time**: 30 minutes

> **Prerequisite**: 

> **Mission**: You understand how to install and use cookie consent manager

## Install and configure cookie consent manager 

### Loadup cookie manager script

On your website, put the following script as close to the head tag as possible

    <script type="text/javascript" src="https://privacy1.gitlab.io/cookie-manager-script/p1-csm.v0.3.84.js"></script>

when this script is loaded, p1ConsentManager object is created as global variable for further use.

### Add the init script

In this script, we need to call p1ConsentManager.init function, there are 5 parameters to be set, 

* colorPrimary: color code banner background.
* fontFamily: your website font family.
* domain: harporcates Privacy Front servcie domain. The default SAAS domain is `cloudfront.privacyone.co`. Use your own Privacy Front domain if you are deploying harpocrates in your own infrastructure.
* inventoryId: the inventory id that can be found in privacy console -> `Cookie Management` tile -> `Cookie Inventories` tab -> `Inventory ID` column
* languageCode: the language code that follows ISO 639-1 standard such as `en` for English

```
 <script>
    window.onload = function(){
        p1ConsentManager.init({
          colorPrimary:"<Color code for banner in the format of #dddddd>",
          fontFamily:"<Your website font family will be applied to text on the banner and settings menu>",
          domain:"<SAAS domain: cloudfront.privacyone.co / Single instance: your own Privacy Front domain>",
          inventoryId:"<Cookie inventory ID underneath your business website domain>",
          languageCode:"<Language code in the format of ISO 639-1 such as 'en' representing English>"
        });
    };

    window.addEventListener('unload', function(event) {
        p1ConsentManager.deinit();
    });
 </script>
```

### Configure data-cookiecategory tag to block cookies

Add data-cookiecategory="cookie category name" to every script that generates cookies, and you need to decide what category this script belongs to, within the 3 supported category types

* functionality
* analytics
* marketing 

When this is configured properly, if someone chooses to block the cookies for this category in Privacy1 consent manager, all cookies generated from the script tagged with this data category will be blocked. 


Check an example here for blocking Google Analytics Cookies

```
<script type="text/plain" data-cookiecategory="analytics" async src="https://www.googletagmanager.com/gtag/js?id=UA-140386914-1"></script>


<script type="text/plain" data-cookiecategory="analytics">
  window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-140386914-1');
</script>

```

### Open consent manager from your code 

To open the consent manager from your code, you can call this function.

```
window.p1ConsentManager.openCookieManager()
```

In certain web builder like wix, it is not possible to access the window object. Add a button on cookie policy page with label `Cookie Preferences` and use the following snippet as custome code to open the cookie manager settings page
```
<script>
  window.addEventListener("DOMContentLoaded",function(){
    let e=document.querySelector('[aria-label="Cookie Preferences"]');
    e.addEventListener("click", function() {
      window.p1ConsentManager.openCookieManager()
    });
  });
</script>

```

