# Harpocrates system overview

> **Read time**: 5 minutes

> **Prerequisite**: You have read the introduction of Harpocrates system.

> **Mission**: Understand Harpocrates system architecture so you can start the deployment.

Privacy1 helps its business clients effortlessly comply with data regulations such as the General Data Protection Regulation (GDPR) or California Consumer Privacy Act (CCPA). 
This document is for data protection officers and software developers. After reading this document the software developers 
should be able to grasp Privacy1's design concepts and start to integrate their services with Harpocrates infrastructure.

## Definitions

### General
* **API** Restful application programming interface used to access Privacy1 backend services.
* **SDK** Language specific tools for accessing Privacy1 backend services.

### Legal
* **GDPR** is European Union [General Data Protection Regulation](https://gdpr-info.eu)
* **CCPA** is [California Consumer Privacy Act](https://leginfo.legislature.ca.gov/faces/billTextClient.xhtml?bill_id=201720180SB1121)
* **Data subject** means an identified or identifiable natural person.
* **Data controller** means the natural or legal person, public authority, agency alone or jointly with others, determines the purposes and means of the processing of personal data.
* **Data processor** means a natural or legal person, public authority, agency which processes personal data on behalf of the data controller.
* **Personal data** means any information relating to an identified or identifiable natural person.
* **Data processing** means ANY operation or set of operations which is performed on personal data
* **Pseudonymization** is a data management and de-identification procedure by which personally identifiable information fields within a data record are replaced by one or more artificial identifiers, or pseudonyms.
* **DPO**: Data Protection Officer.

### Harpocrates infrastructure

Harpocrates is Privacy1's core data privacy protection infrastructure. It comprises the following services or modules.

* **Privacy Manager**: Privacy Manager is a generic end-user facing service that end users use to manage their privacy settings. Business can choose to use the Privacy Manager developed Privacy1 or developing its own version using Harpocrtes APIs.
* **Privacy Front**: Privacy Front receives all requests from Privacy Manager. It acts as a gateway of Harpocrates infrastructure and forward the requests to upstream services if necessary.
* **Cerberus**: Cerberus service is the authentication and authorization system of Harpocrates infrastructure. The user types in Cerberus includes USER and SERVICE. A USER account is for a DPO, a data engineer or an administrator. A SERVICE account is an identifier of a service or an application. Cerberus implements a role-based authorization system.
* **Keychain**: Keychain service is the core service for consent management and pseudonymization. The service categorizes a user's data and encrypts the data with a key specific for that category. This unique patented pseudonymization algorithm is inherently suitable for the implementation of many requirements of data regulations, such as right to erasure, security of processing, etc.
* **Keychain Analytics**: Keychain Analytics service defines a controlled multi-step workflow for data engineers and data engineers/scientists to process and analyze end user's data. The service keeps the original data or interim data encrypted if stored on a permanent storage between two adjacent data processing. A DPO also needs to approve any data access.
* **LDAR**: LDAR stands for Legal Data Access Request and is pronounced as `le dar`. This service manages the legal agreements, such as End User License Agreements (EULA) or Privacy Policy, and records if end users agree to those agreements. LDAR service also handles subject access requests submitted by end users. End users have the right to download the information processed by a service provider they use.
* **DPIA**: This is the module where your business can record data processing activities and conduct privacy risk analysis and data impact assessment.
* **Privacy Console**: Privacy Console is a web portal for DPOs, data engineers and administrators to manage different aspects of Privacy1's infrastructure. DPOs process subject access requests and internal analytics data access requests. Data engineers and data scientists request analytics data access. Administrations perform tasks such as adding new accounts, roles, data categories, etc.

If we put all system components into the original design of our privacy protection infrastructure, the system overview look like the following diagram.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/intro/system_overview.png" alt="system overview" width="800" height="450"/>


## Take it from here

Data privacy protection is essential to your business. You have decided to carry on and integrate Harpocrates. The information 
above has a lot of legal words and tech conceptsand may sound alienating. We guarantee our two-hour [Data Privacy Training](https://www.privacyone.co/training) programs will
make legal requirements and technology best practise crystal clear and you will be able to make sense how to implement all these.

We also have professional consultants that are committed to helping your business implement the system. Book them [here](https://www.privacyone.co/contact).

The next tutorial talks about deploying Harpocrates to your cloud service provider or on-premises servers. If you are in a legal 
role, leave the next step to your technology people. We will come out and meet you in Golden Path tutorial.