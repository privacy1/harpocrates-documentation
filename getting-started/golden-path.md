# Phase I - Golden path to data regulatory compliance and data security enforcement

> **Read time**: 30 minutes

> **Prerequisite**: Harpocrates has been deployed

> **Mission**: Your business has a data compliance and data security system up and running. 
> End users in your system will be able to exercise privacy rights
> * Manage privacy policy agreements
> * Grant/revoke consents 
> * Object/restrict data processing
> * Delete data
> * Download data
>
> Businesses have a solid foundation of data privacy management, a transparent data processing paradigm and get all data users a clear picture how to implement *Privacy By Design*. 
>  * Divided roles that are accountable for different data processing activities
>  * Manage privacy rights requests by data administrators
>  * Audit privacy rights requests by DPO or the legal team
>  * Statistics and insights of privacy rights requests by DPO or the legal team
>  * A functional and transparent Workflow for internal data processing activities among tech and legal teams
>  * Conduct data processing activity bookkeeping
>  * Conduct risk analysis and data impact assessment

## Overview
Golden path is the first phase of Harpocrates integration. It is meant to lay down the foundation of your data 
compliance and data security protection work quickly. It is an exercise that takes a few hours to complete. Please read 
our introduction if you haven't already to get a basic understanding of data compliance and data security work. This 
tutorial will guide you through all the steps of needed during the first phase of integration. Golden path provides your 
business the best way to demonstrate data regulatory compliance such as the purposes of your data processing, what kind 
of data you process, who has access to it in your organization and to what extend the data is being processed.

## Integrate privacy manager
Privacy Manager is a toolbox used by your business end users, also known as `data subjects` for their data privacy 
rights exercises. They can exercise the following privacy rights from Privacy Manager. 

* Manage privacy policy agreements
* Grant/revoke data processing consents 
* Object data processing
* Restrict/Unrestrict data processing
* Delete data
* Download data

The end user-facing frontend, Privacy Manager is served from Privacy Front service and the UI components enabled in 
Privacy Manager can be customized through a configuration property in Privacy Front service. Choose the components you 
would like to have for your business by keeping the components in the configuration list. The ones that are removed will 
not be displayed in Privacy Manager UI.

```
# Tweak the following configuration to customize the UI components you would like to have for your Privacy Manager
privacyone.privacymanager.enabledComponents=DataPolicies,DataRestriction,DataDownload,DataDeletion,DataConsent
```

Now the configuration is all set, in order to access Privacy Manager you only need to copy the following code into a web 
component of your frontend code. You need to provide an authentication token that identifies a user. The way to pass a token to Privacy Manager is through the parameter by invoking the Privacy Front URL as described in the following 
code block:
 
```
<iframe
  src="https://<PRIVACY_FRONT_DOMAIN>:8443/privacymanager?token=<USER_SYSTEM_AUTHORIZATION_TOKEN>"
  width="1200"
  height="800"
></iframe>
```

The `PRIVACY_FRONT_DOMAIN` parameter is your Privacy Front service domain name. Its setup is described in the
previous chapter - deployment. You must prepare an officially signed SSL certificate for Privacy Front to support SSL. A
recommended domain to bind the certificate to is privacy.<YOUR_BUSINESS_DOMAIN>.

The `USER_SYSTEM_AUTHORIZATION_TOKEN` parameter is a oAuth token or any other type of user token used in your system to
identify a user. You pass that from your website or mobile application to Privacy Front service.

There is a test token `91cb3135-3431-4d9e-8493-0c2aa346aed4` assigned to a test account `aaa` in Harpocrates. You should
be able to use it to validate all the following steps before you import your actual users into Harpocrates.

To validate your deployment, you simply put the following URL with the deployed domain in your browser, Privacy Manager 
should be loaded up.
```
https://<PRIVACY_FRONT_DOMAIN>:8443/privacymanager?token=91cb3135-3431-4d9e-8493-0c2aa346aed4
```

## Integrate user authentication
When the frontend part is wired up, you need to setup the backend part of Harpocrates system to be able to identify your
users. The token sent from Privacy Manager and received by Privacy Front will be forwarded to a backend authentication
endpoint implemented by your backend system. All it does is to identify an oAuth token or a user token and exchange with a
valid user ID stored in your backend system. From this point in time, Harpocrates will use the user ID internally to 
represent your user. That is the only information Harpocrates knows about your user, nothing else.

Your user backend system needs to setup this authentication endpoint that can handle http request described
in the following format:

### URL 
`http://<AUTHENTICATION_ENDPOINT_URL>`

Configure this URL in Privacy Front configuration property `privacyone.privacyfront.external.authentication.url` field
Refer to the `Operation Handbook` documentation on how to operate on configuration changes in either Docker or 
Kubernetes environment.

### Header
`Authorization: Bearer <USER_SYSTEM_AUTHORIZATION_TOKEN>`

The request uses a standard authorization header described in [RFC 6750](https://tools.ietf.org/html/rfc6750).

### Method 
`GET`

### Response
`200`: A valid `USER_ID` in string that is going to be used in Harpocrates to identify your users.

`403`: with detailed error message if authentication token is invalid.


## Define your business privacy policy
Congratulations! Up to this point, you have Harpocrates deployed and integrated. Now it is time to customize the system
based on your data processing activities. The first step is to configure your data privacy policy.

1. Login Privacy Console under the administrator role.
2. Click the **Data Tranparency** button in Privacy Console landing page.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/data_transparency.png" alt="data transparency" width="600"/><br/>

3. Click the **Legal Document** tab in the side bar.
4. Click the **CREATE PRIVACY POLICY** button and fill in all the fields that are required.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/create+privacy+policy.png" alt="Create privacy policy" width="400"/><br/>

`Version` is the version of your privacy policy. It is an incremental number. 

`Content` is the content of your privacy policy in markdown format. The editor helps your administrator create the 
content of privacy policy with preview next to it.


5. After a privacy policy is defined, it first is created as a draft version. Keep revising on it until you are satisfied.
To make it publicly avaialble for the data subjects (your endusers), an administrator clicks the `Publish` button. When a new version 
is published, all previous versions will be set into deprecated mode. The content of each version is protected by a content hash
which is presented to the data subjects.

6. You can validate accessing it with the test token by impersonating the `aaa` account.
Load up Privacy Manager from your application that has incorporated Privacy Manager. Click the Agree button. You have
completed this step. A privacy policy is agreed and takes effect for your users.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/privacy_policy_agreed.png" alt="Legal policy agreed" width="600"/><br/>

7. Your business now keeps the proof that a user has agreed on a certain version of privacy policy. Since the content of a policy is
subject to changes, both your business and a data subject can use the content hash as the proof of a policy agreement that is in effect.

## Define your data categories

### Introduction
Good work! You are one step further towards the data regulatory compliance. Data regulations all require businesses to
well-define the categories of personal data and keep it as part of the data processing activities. Precisely defining
data categories not only ensures your data processing is lawful and more transparent, but also strengthens the trust 
between your business and your end users. Your customers will be more confident to use your service and willing to provide 
additional personal data because they have a choice to control (restrict | object | delete) the usage of certain data 
categories they care and deem sensitive.

### Legal background
* Define data categories in combination with your data processing activities or data usage purpose. For example, a 
location data category can mean your business collects IP addresses or Geo-location information from a user. A target 
adverts category can be used to define the purpose of your data usage.
* The more granular you define a data category, the more transparency you provide to your end users and more controls 
you give back to them on their data.
* There is a predefined Primary data category that a user can not choose to opt out. This category serves the purpose of
holding all principal data such as Email account or credit card information that your service will fail serving a 
customer without such information.
* Be very specific and make it easy to understand in the data category definition text. This is going to be seen by your 
end users.
* Special categories like health data, sex life, political view and religious belief should be defined if they are part of
your data processing activities. Refer to Article 9 of GDPR.

### How to steps
1. Login Privacy Console under the administrator role.
2. Click the **Keychain** button in Privacy Console landing page.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/keychain.png" alt="Keychain" width="600"/><br/>

3. Click the **Data Categories** tab in the side bar.
4. Click the **CREATE DATA CATEGORY** button and fill in all the fields that are required.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/new_category.png" alt="New data category" width="400"/><br/>

5. When data categories are defined, you can validate accessing them with the test token by impersonating the `aaa` account.
Load up Privacy Manager from your application that has incorporated Privacy Manager. You as the user represented by the `aaa`
account will be able to exercise privacy rights granted by the laws. 

6. Click **Restrict my data** tab and then use individual data category controls to start objecting or restricting data
processing activities. At this phase You will have to handle all data restriction | objection requests in your system by
yourself. Read Phase II to understand how to automate data restriction | objection throughout your data processing activities.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/restrict_data.png" alt="Restrict data" width="600"/><br/>

7. Click **Consent management** tab and then use individual data category controls to start DELETING user data from a 
specific data category. When a consent is revoked, a user indicates that his/her data on that category should all be 
deleted. At this phase You will have to deal with the deletion in your system by yourself. Read Phase II to understand 
how to automate data deletion throughout your data processing activities.

## Define your service products

### Introduction
Bravo! More trust and more transparency accomplished. According to data regulations, your end users have the right to obtain
from the your business the personal data being processed. In this section, we will assist you to wire your system up with
our LDAR system to achieve the goal of fulfilling the data access requests with the follow information:

* The purposes of the processing.
* The categories of personal data concerned.
* The recipients or categories of recipient to whom the personal data have been or will be disclosed, in particular 
recipients in third countries or international organisations.
* The planned duration of storage or criteria for personal information.

### Legal background
We have defined the date category in last step. In this step you need to define the actual product names that are involved
in your business data collection, storage, processing and transfer and your data subject can obtain a copy from your service.

If your business is processing large volume of personal information, data subjects will be able to specify their request
within the right of access regarding specific data processing or kind of information.

### How to steps
1. Login Privacy Console under the administrator role.
2. Click the **LDAR** button in Privacy Console landing page.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/data_transparency.png" alt="data transparency" width="600"/><br/>

3. Click the **Data Products** tab in the side bar.
4. Click the **CREATE DATA PRODUCT** button and fill in all the required fields. In the description, you shall 
   provide the data subject with all of the information listed in the `Introduction` section above.

<img src="https://s3.eu-north-1.amazonaws.com/com.privacyone.doc/harpocrates/phase-I/new_data_product.png" alt="New data product" width="400"/><br/>

5. When all data products are defined, you can validate exercising data subject access requests by using Privacy Manager 
**Download my data** feature. Once again, we use the test token by impersonating the `aaa` account. You as the user represented
by the `aaa` account will be able to start initiating subject access request to obtain a copy of its data.

## Golden path mission complete!

Brilliant! You have succeeded in golden path phase I. Your business has established a solid foundation of data privacy 
management, a transparent data processing paradigm and get all data users in your business a clear picture how to
implement Privacy By Design.


Data subjects in your system will be able to exercise privacy rights

* Manage privacy policy agreement
* Grant/revoke consents
* Object/restrict/unrestrict data processing
* Delete data
* Download data

Your business as a data controller or data processor will have

* Divided roles that are accountable for different data processing activities
* Manage privacy rights requests by data administrator
* Audit privacy rights requests by DPO or your legal team
* Statistics and insights of privacy rights requests can be analyzed by DPO or your legal team
* A functional and transparent Workflow for internal data processing activities among tech and legal teams

## What's next
Golden path is a significant step towards data compliance. Furthermore it lays down a great foundation for your privacy 
protection work. However, it is only a framework. To convert this first victory to the final success, our well-designed 
phase II Privacy by Design documentations will lead you to go through a number of practices to incorporate Privacy by Design 
and highest security enforcement in your business.

To view Privacy by Design documentations, please purchase a commercial license on the [License](https://app.privacyone.co/license/purchase) page.

In this next phase, your business will incorporate data privacy protection throughout the whole engineering process with 
some of the highlights.

* Data privacy management is all automated
* Data governance is all streamlined
* Data usages are all separated to authorised users or services account with auditing in place
* Workflow among tech and legal teams and the actual data usage
* Data processing activities recording
* Privacy risk analysis
* Data protection impact assessment
* Data security is enforced to the highest level
