# A Tutorial for data privacy protection in AWS Cognito


> **Read time**: 10 minutes

> **Prerequisite**: 
>  1. Understanding of how to use Cognito, lambda function and API gateway in AWS 
>  2. Understanding of how to use Privacy1 Harpocrates infrastructure

> **Mission**: 
>  1. Integrate Harpocrates with AWS Cognito              
>  2. Your business backend system including Cognito meet data regulatory requirments

## Legal background
GDPR requires data system to have Privacy by Design and Privacy by Default. Harpocrates has implemented all general principles that include data transparency, 
data limitation, data impact assessment etc into the framework. To understand the overall design paradigm, go to [Harpocrates documentation](https://app.privacyone.co/documentation)
to read more. There are two primary tasks in this tutorial which is to integrate Harpocrates with your AWS Cognito system and make sure your new user registration flow is protected by Harpocrates infrastructure. Finally a bonus task to teach you how to pseudonymize personal data in your business system.

Below is an architecture diagram showing the connections between components


## Task 1 Integrate Harpocrates with AWS Cognito

First check "integrate user authentication" part of Privacy1 Golden Path documentation,  the goal of this task is to use a valid rest API token and exchange with userId in your user system.

### Create a lambda function that validates the token. 

Our implementation follows aws instruction documentation [How to verify JWT token](https://aws.amazon.com/premiumsupport/knowledge-center/decode-verify-cognito-json-token/) 
and code example [here](https://github.com/awslabs/aws-support-tools/tree/master/Cognito/decode-verify-jwt).

```
const util = require("util");
const axios = require("axios");
const jsonwebtoken = require("jsonwebtoken");
const jwkToPem = require("jwk-to-pem");

// change cognitoPoolId and cognitoRegion to proper value 
const cognitoPoolId = "<cognito pool id>";
const cognitoRegion = "<cognito region>";

if (!cognitoPoolId || !cognitoRegion) {
  throw new Error("env var required for cognito pool and region");
}
const cognitoIssuer = `https://cognito-idp.${cognitoRegion}.amazonaws.com/${cognitoPoolId}`;

let cacheKeys;
const getPublicKeys = async () => {
  if (!cacheKeys) {
    const url = `${cognitoIssuer}/.well-known/jwks.json`;
    const publicKeys = await axios.default.get(url);

    cacheKeys = publicKeys.data.keys.reduce((agg, current) => {
      const pem = jwkToPem(current);
      agg[current.kid] = { instance: current, pem };
      return agg;
    });
    return cacheKeys;
  } else {
    return cacheKeys;
  }
};

const verifyPromised = util.promisify(jsonwebtoken.verify.bind(jsonwebtoken));

const tokenVerifyHandler = async (token) => {
  let result;
  try {
    console.log(`user claim verfiy invoked`);

    const tokenSections = (token || "").split(".");
    if (tokenSections.length < 2) {
      throw new Error("requested token is invalid");
    }
    const headerJSON = Buffer.from(tokenSections[0], "base64").toString("utf8");
    const header = JSON.parse(headerJSON);
    const keys = await getPublicKeys();
    const key = keys[header.kid];
    if (key === undefined) {
      throw new Error("claim made for unknown kid");
    }
    const claim = await verifyPromised(token, key.pem);
    const currentSeconds = Math.floor(new Date().valueOf() / 1000);
    if (currentSeconds > claim.exp || currentSeconds < claim.auth_time) {
      throw new Error("claim is expired or invalid");
    }
    if (claim.iss !== cognitoIssuer) {
      throw new Error("claim issuer is invalid");
    }
    if (claim.token_use !== "access") {
      throw new Error("claim use is not access");
    }
    result = {
      userName: claim.username,
      clientId: claim.client_id,
      isValid: true,
    };
  } catch (error) {
    result = { userName: "", clientId: "", error, isValid: false };
  }
  return result;
};

exports.handler = async (event) => {
   let response = {
    statusCode: null,
    body: null,
  };
  
  const authHeader = event.headers["Authorization"];
  let token,userName;
  if (authHeader.startsWith("Bearer ")) {
    token = authHeader.substring(7, authHeader.length);
    const result = await tokenVerifyHandler(token);
    if(result.isValid) {
      response = {
        statusCode: 200,
        body: result.userName,
      };
    } else {
       response = {
        statusCode: 401,
        body: "token not valid",
      };
    }
    
  } else {
    //Error
    console.log("auth header not in right format");
  }

  return response;
};

```

### Connect Lambda to API gateway

The next step is to create a resource with GET method in the API gateway and connect it to this new Lambda function.

### Configure the authentication open url in Harpocrates

Read **Integrate user authentication** part of [Harpocrates Golden Path](https://app.privacyone.co/documentation) and the [Privacy Front Service handbook documentation](https://app.privacyone.co/documentation) on how to configure Harpocrates Priavcy Front on the external authentication endpoint you just created and enabled in the previous steps.


## Task 2 Make sure new user registration is protected by Harpocrates infrastructure 

In cogito you can add a lambda function which is triggered by different events. Here we want to add users to Harpocrates during 
the new user registration flow. Then make that happen, we can use a post confirmation trigger. In our example we created a lambda 
function "cognito PostConfirmation" connected to post confirmation trigger .  

### Create a Lambda function that adds user to Harpocrates

```
const harpoSDK = require("p1-harpocrates-sdk");

// change follow 5 variables to proper values
const DOMAIN_KEYCHAIN = "<harpocrates keychain service domain>";
const DOMAIN_CERBERUS = "<harpocrates cerberus service domain>";
const KEYCHAIN_API_KEY = "<API KEY for keychain>"; //32 bytes,
const CERBERUS_USERNAME = "cerberus service account username";
const CERBERUS_PWD = "cerberus service account password";

const keychain = new harpoSDK.Keychain(DOMAIN_KEYCHAIN, 8090, KEYCHAIN_API_KEY, 30);

exports.handler = async (event, context, callback) => {
  
  await keychain
  .authenticate(DOMAIN_CERBERUS, 8093, CERBERUS_USERNAME, CERBERUS_PWD)
  .then(() => {
    console.log("harpo keychain auth done");
  })
  .catch((err) => {
    console.log("harpo keychain auth got error", err);
  });
  
  console.log("post confirm event:", event);
  const { userName } = event;
  const { email } = event.request.userAttributes;
  console.log("userName:", userName);
  console.log("email:", email);

  if(userName) {
    await keychain.createUser(userName);
    console.log("user created");
    callback(null, event);
  } else{
    callback(null, event);
  }

};

```

### Add post confirmation trigger in Cognito connected to the lambda function created above


## Bonus task Data pseudonymization in your business data system

At the last step of this tutorial, we create an example to use Javascript p1-harpocrates-sdk in lambda to pseudonymize your business personal data. 

### Example code of a Lambda function that saves or reads data from a data storage

```
const harpoSDK = require("p1-harpocrates-sdk");

// change follow 5 variables to proper values
const DOMAIN_KEYCHAIN = "<harpocrates keychain service domain>";
const DOMAIN_CERBERUS = "<harpocrates cerberus service domain>";
const KEYCHAIN_API_KEY = "<API KEY for keychain>"; //32 bytes
const CERBERUS_USERNAME = "cerberus service account username";
const CERBERUS_PWD = "cerberus service account password";

const keychain = new harpoSDK.Keychain(DOMAIN_KEYCHAIN, 8090, KEYCHAIN_API_KEY, 30);

function createResponse(code, body) {
  return {
    statusCode: code,
    headers: {
      "Access-Control-Allow-Origin": "*"
    },
    body: JSON.stringify(body),
  }
}

const saveData = async (userId, data) => {
  const { address } = data;

  try {
    const encryptedBytes = await keychain.encryptString(
      userId,
      "<YOUR_DATA_CATEGORY>",
      address
    );
    const result = await saveToDb(encryptedBytes);
  } catch (e) {
    console.log(e);
    return createResponse(500, e);
  }

};

const saveToDb = async (data) => {
  // do db work
  console.log("simulate save to db operation : ",data);
};


exports.handler = async (event) => {
  const { userId, data } = JSON.parse(event.body);
  return await saveData(userId, data);
};

```
