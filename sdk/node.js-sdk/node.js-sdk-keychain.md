# Node.js SDK Keychain

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host
* Keychain port default 8090
* Service key a service specific key that is used to derive the per user per service crypto key
* Cache expiration time which specifies how long time in seconds an encryption key is cached locally in the SDK instance

Every service communicating with the Keychain service should initialize a SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
class Keychain{
 /**
  * Constructor
  *
  * @param {String} keychainHost keychain host address
  * @param {int} keychainPort keychain host port. The factory setting is 8090
  * @param {String} serviceKey the service key for keychain key derivation
  * @param {int} cacheExpireInSeconds consent key cache expiration in seconds
  */
  constructor(keychainHost, keychainPort, serviceKey, cacheExpireInSeconds)
}
```

Code Example

```
const KEYCHAIN_HOST = "65.2.93.239";
const KEYCHAIN_PORT = 8090;
const YOUR_SERVICE_CRYPTO_KEY = "DD5FB57EFEAF7AA44FCF34C378461033";
const KEYCHAIN_CACHE_EXPIRE = 30;

const keychain = new Keychain(
        KEYCHAIN_HOST,
        KEYCHAIN_PORT,
        YOUR_SERVICE_CRYPTO_KEY,
        KEYCHAIN_CACHE_EXPIRE);
```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  /**
   * Authenticate to Cerberus system.
   *
   * First you need to register a service account into Cerberus system for your backend micro-service
   * that needs access to keychain for personal data pseudonymisation. If you have multiple services
   * that need access to keychain, it is recommended to register multiple accounts for keychain access.
   *
   * This method should be called before any other APIs.
   *
   * @param {String} cerberusHost authentication host address
   * @param {int} cerberusPort authentication host port. The default setting is 8093
   * @param {String} accountName the registered backend micro-service account name
   * @param {String} accountPassword the registered backend micro-service account password
   *
   * @returns {Promise}
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   authenticate(cerberusHost, cerberusPort, accountName, accountPassword): Promise
```

Code Example

```
const CERBERUS_HOST = "65.2.93.239";
const CERBERUS_PORT = 8093;
const CERBERUS_SERVICE_ACCOUNT_USERNAME = "EXAMPLE_USERNAME";
const CERBERUS_SERVICE_ACCOUNT_PWD  = "EXAMPLE_PWD";

keychain
    .authenticate(CERBERUS_HOST, CERBERUS_PORT, CERBERUS_SERVICE_ACCOUNT_USERNAME, CERBERUS_SERVICE_ACCOUNT_PWD)
    .then(() => {
      console.log("authenticate done");
    })
    .catch(err => {
      // handle error
      console.log("authenticate error",err);
    });

```

## User operations
You shall hookup Keychain API with your user registration flow so that Keychain service will be notified for each new 
user registration as well as existing user deletion. A User ID is the only information you need to pass down into Keychain 
service.

### Create user

```
  /**
   * Create a user with userId into keychain system. Invoke this API within your
   * new user registration process.
   *
   * @param {String} userId a user id that identifies a user in your system
   *
   * @returns {Promise} 
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
  createUser(userId):Promise

```

Code Example

```
keychain
    .createUser("used_id")
    .then(data => {
      console.log("user created");
    })
    .catch(err => {
      // handle error;
      console.log("createUser error",err);
    });
```

### Delete user

```
 /**
  * Soft or hard delete user by its user ID. Always execute a soft delete first. This
  * API serves as an archive functionality. After soft delete, subsequent API calls won't
  * be able to retrieve the user data. After a cool-off period you then execute
  * hard delete programatically or manually from Backstage software. A hard delete will
  * purge a user's data from the keychain database.
  *
  * @param {String} userId
  * @param {boolean} trueDelete true if it is a hard delete, false otherwise
  *
  * @returns {Promise} 
  *
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
   deleteUser(userId, trueDelete):Promise
```

## Consent operations
A consent represents the ultimate permission a user has given your business for personal data processing if your legal basis 
is consent based.

### Read consents

```
 /**
  * Get a user's consents
  *
  * @param {String} userId a user id that identifies a user in your system
  *
  * @returns {Promise} a promise object that has user's consents choice in json format when completed
  * {
  *   "userId": "string"",
  *   "consents": [
  *     {
  *       "categoryTitle": "string",
  *       "granted": boolean
  *     },
  *     {
  *       ....
  *     }
  *   ]
  * }
  *
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  getConsent(userId):Promise

```

### Write a single consent
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

**NOTE: Withdrawing a consent = restrict future data processing + historical data deletion** 



```
  /**
   * Set a user consent for a specific data category
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} categoryTitle a consent category title in string for consent settings change
   * @param {boolean} granted true if a consent is granted for that specific category false otherwise
   *
   * @returns {Promise}
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   setConsent(userId, categoryTitle, granted):Promise

```

### Write multiple consents
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

**NOTE: Withdrawing a consent = restrict future data processing + historical data deletion** 



```
  /**
   * Set a user's consents for multiple data categories
   *
   * @param {JSON} consents a user's consents choice in json format
   * {
   *   "userId": "string"",
   *   "consents": [
   *     {
   *       "categoryTitle": "string",
   *       "granted": boolean
   *     },
   *     {
   *       ....
   *     }
   *   ]
   * }
   *
   * @return {Promise}
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   setConsents(consents):Promise
```

## Restriction operations
### Read restriction

```
 /**
  * Get user restrictions
  *
  * @param {String} userId a user id that identifies a user in your system
  *
  * @return {Promise} a promise object that has user's restrictions choice in json format when compelte
  * {
  *   "userId": "string"",
  *   "restrictions": [
  *     {
  *       "categoryTitle": "string",
  *       "restricted": boolean
  *     },
  *     {
  *       ....
  *     }
  *   ]
  * }
  *
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  getRestriction(userId):Promise

```

### Write a single restriction
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`


```
 /**
  * Set a user's restriction for a specific data category
  *
  * @param {String} userId a user id that identifies a user in your system
  * @param {String} categoryTitle a data category title in string for restriction settings change
  * @param {boolean} restricted true if a data category is restricted for processing false otherwise
  *
  * @return {Promise}
  *
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  setRestriction(userId, categoryTitle, restricted):Promise
```

### Write multiple restrictions
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`


```
 /**
  * Set a user's restrictions for multiple data categories
  *
  * @param {Promise} a promise object that has a user's restrictions choice in json format
  * {
  *  "userId": "string"",
  *  "restrictions": [
  *    {
  *      "categoryTitle": "string",
  *      "restricted": boolean
  *    },
  *    {
  *      ....
  *    }
  *  ]
  * }
  *
  * @return
  *
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  setRestrictions(restrictions):Promise
```

## Deletion operations
**NOTE: deleting data is permanent**. This API will be triggered by a data subject from within Privacy Manager, 
should you choose to allow him/her carry out such an action. Otherwise, your backend services can trigger it periodically.

### Delete user personal data from a single data category
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`


```
  /**
   * Set a user's deletion request for a specific data category
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} categoryTitle a data category title in string for deletion
   *
   * @return {Promise}
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   setDeletion(userId, categoryTitle):Promise
```

### Delete user personal data from multiple data categories
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`


```
  /**
   * set user's delete request for multiple data categories
   *
   * @param {Promise} a promise object that has a user's deletion request in json format
   * {
   *  "userId": "string",
   *  "deletionCategoryTitles": ["categoryTitile1", "categoryTitle2", ...]
   * }
   *
   * @return
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   setDeletions(deletions):Promise
```

## Client-side crypto operations
Cryptographic execution inside the SDK. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server 
side crypto operation tradeoffs.

### Encrypt a string
```
  /**
   * Encrypt a String plaintext
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} plaintextString a plaintext string that needs to be encrypted
   *
   * @return {Buffer | Uint8Array} a ciphertext that is a encrypted byte array
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
   encryptString(userId, category, plaintextString):Promise
```

### Decrypt to a string
```
  /**
   * Decrypt a ciphertext to String
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} ciphertextBytes a ciphertext bytes that needs to be decrypted
   *
   * @return {String} a String plaintext
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
   decryptToString(userId, category, ciphertextBytes):Promise
```

### Encrypt a number
```
 /**
  * Encrypt a Number plaintext
  *
  * @param {String} userId a user id that identifies a user in your system
  * @param {String} category a data category that the plaintext data belongs to
  * @param {String} plaintextNumber a plaintext number that needs to be encrypted
  *
  * @return {Buffer | Uint8Array} a ciphertext that is a encrypted byte array
  *
  * @throws Error with a specific cryptographic error message can be cought in try / catch block
  */
  encryptNumber(userId, category, plaintextNumber):Promise
```

### Decrypt to a number
```
  /**
   * Decrypt a ciphertext to Number
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} ciphertextBytes a ciphertext bytes that needs to be decrypted
   *
   * @return {Number} a Number plaintext
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
  decryptToNumber(userId, category, ciphertextBytes):Promise
```

### Encrypt a boolean
```
  /**
   * Encrypt a boolean plaintext
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} plaintextBoolean a plaintext boolean that needs to be encrypted
   *
   * @return {Buffer | Uint8Array} a ciphertext that is a encrypted byte array
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
  encryptBoolean(userId, category, plaintextBoolean):Promise
```

### Decrypt to a boolean
```
  /**
   * Decrypt a ciphertext to boolean
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} ciphertextBytes a ciphertext bytes that needs to be decrypted
   *
   * @return {boolean} a boolean plaintext
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
  decryptToBoolean(userId, category, ciphertextBytes):Promise
```

### Encrypt a date object

```
 /**
  * Encrypt a date plaintext
  *
  * @param {String} userId a user id that identifies a user in your system
  * @param {String} category a data category that the plaintext data belongs to
  * @param {String} plaintextDate a plaintext date that needs to be encrypted
  *
  * @return {Buffer | Uint8Array} a ciphertext that is a encrypted byte array
  *
  * @throws Error with a specific cryptographic error message can be cought in try / catch block
  */
  encryptDate(userId, category, plaintextDate):Promise
```

### Decrypt to a date object
```
  /**
   * Decrypt a ciphertext to Date
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} ciphertextBytes a ciphertext bytes that needs to be decrypted
   *
   * @return {Date} a Date plaintext
   *
   * @throws Error with a specific cryptographic error message can be cought in try / catch block
   */
   decryptToDate(userId, category, ciphertextBytes)
```

## Server-side crypto operations
Server side cryptographic execution. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server side
crypto operation tradeoffs.

### Encrypt a string
```
  /**
   * Perform encryption of a String plaintext by keychain service
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} plaintextString a plaintext string that needs to be encrypted
   *
   * @return {Buffer | Uint8Array} a ciphertext that is a encrypted byte array
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   encryptStringFromService(userId, category, plaintextString):Promise
```

### Decrypt to a string
```
  /**
   * Perform decryption of a ciphertext to String by keychain service
   *
   * @param {String} userId a user id that identifies a user in your system
   * @param {String} category a data category that the plaintext data belongs to
   * @param {String} ciphertextBytes a ciphertext bytes that needs to be decrypted
   *
   * @return {String} a String plaintext
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   decryptToStringFromService(userId, category, ciphertextBytes):Promise
```
