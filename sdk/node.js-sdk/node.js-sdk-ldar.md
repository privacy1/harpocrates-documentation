# Node.js SDK LDAR

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

* LDAR host
* LDAR port default 8091

Every service communicating with the LDAR service shall initialize a SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
class Ldar {
  /**
   * Constructor
   * 
   * @param {String} ldarHost ldar host address
   * @param {int} ldarPort ldar host port. The factory setting is 8091
   */
   constructor(ldarHost, ldarPort)
```

Code Example
```
const LDAR_HOST = "65.2.93.239";
const LDAR_PORT = 8091;

const ldar = new SDK.Ldar(LDAR_HOST, LDAR_PORT);
```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  /**
   * Authenticate to Cerberus system. 
   *
   * First you need to register a service account into Cerberus system for your backend 
   * micro-service that needs access to ldar for handling your enduser's legal and data 
   * access requests.
   *
   * This method should be called before any other APIs. Out of factory setting, you can
   * create a new account with a role `business.service` from cerberus to perform all
   * subsequent api calls. Upon the details of your system architecture, you can create
   * roles for micro-services that perform different tasks. For example a role for legal 
   * request processing and a seperate role for data request process.
   *
   * @param {String} cerberusHost authentication host address
   * @param {int} cerberusPort authentication host port. The factory setting is 8093
   * @param {String} accountName the registered backend micro-service account name
   * @param {String} accountPassword the registered backend micro-service account password
   *
   * @returns {Promise}
   *
   * @throws error can be caught in promise .catch chain. Use error.response.status to check the error code
   */
   authenticate(cerberusHost, cerberusPort, accountName, accountPassword):Promise
```

Code Example
```
const CERBERUS_HOST = "65.2.93.239";
const CERBERUS_PORT = 8093;
const CERBERUS_SERVICE_ACCOUNT_USERNAME = "EXAMPLE_USERNAME";
const CERBERUS_SERVICE_ACCOUNT_PWD = "EXAMPLE_PWD";

ldar
  .authenticate(
    CERBERUS_HOST,
    CERBERUS_PORT,
    CERBERUS_SERVICE_ACCOUNT_USERNAME,
    CERBERUS_SERVICE_ACCOUNT_PWD
  )
  .then(() => {
    console.log("ldar auth ok");
  });

```

## Data subject access requests operations
The following APIs are used to control the data subject access request processing flow. The workflow are divided into the
following phases.

* REQUESTED
* PROCESSING
* PROCESSED
* FILE_READY

Read `PhaseII/Subject Access Request` for more the design details.

### Read all requested data subject access requests (DARs)
Once a data subject access request has been registered through Privacy Manager, you backend API can start to track all 
pending requests from invoking this API. If you are processing access requests in batch pipeline, it is recommended to 
query them every day.

```
  /**
   * Get all requested data access requests
   *
   * @param
   *
   * @returns {Promise} a promise that has all requested data access requests in json format when completed
   * [
   *   {
   *     darId: 'efefefe111',
   *     statusTitle: 'REQUESTED',
   *     stateDescription: 'description about this dar',
   *     "products": [
   *       {
   *         "productId": 1,
   *         "productTitle": "email",
   *         "productDescription": "all your email in plaintext"
   *       },
   *       ...
   *     ],
   *     "archive": {
   *       "archiveId": 5,
   *       "archiveSize": "1GB",
   *       "archiveType": ".zip",
   *       "archiveExpirationDate": "1970-01-08T00:00:00Z"
   *     }
   *   },
   *   {
   *    ...
   *   }
   * ]
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   getRequestedDars():Promise
```

### Read a single data subject access request
```
  /**
   * Get a data access request by its dar id
   *
   * @param {String} darId a data access request ID
   *
   * @returns {Promise} a promise that has all requested data access requests in json format when completed
   *
   * {
   *   "statusTitle": "requested",
   *   "stateDescription": "User data access request has been registered.",
   *   "products": [
   *     {
   *       "productId": 1,
   *       "productTitle": "email",
   *       "productDescription": "all your email in plaintext"
   *     },
   *     ...
   *   ],
   *   "archive": {
   *     "archiveId": 5,
   *     "archiveSize": "1GB",
   *     "archiveType": ".zip",
   *     "archiveExpirationDate": "1970-01-08T00:00:00Z"
   *   }
   * }
   *
   * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
   */
   getDarById(darId):Promise
```

### Transition the workflow to PROCESSING state
```
 /**
  * Transition the data access request state into the processing state. Your data collection pipeline
  * can perform state transition as the following:
  *
  * REQUESTED -> PROCESSING
  *
  * @param {String} darId a data access request ID
  * @param {String} stateDescription a description about the new state that is transitioned into
  * @returns {Promise}
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  processingDar(darId, stateDescription):Promise
```

### Transition the workflow to PROCESSED state
```
 /**
  * Transition the data access request state into the processed state. Your data collection pipeline
  * can perfom the following state transitions:
  *
  * PROCESSING -> PROCESSED_SUCCESS
  * PROCESSING -> PROCESSED_FAILURE
  * PROCESSED_FAILURE -> PROCESSED_SUCCESS
  *
  *
  * @param {String} darId a data access request ID
  * @param {String} status a status can be either PROCESSED_SUCCESS or PROCESSED_FAILURE depending on
  *                        the real data collection pipeline results
  * @param {String} stateDescription a description about the new state that is transitioned into
  * @returns {Promise}
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  */
  processedDar(darId, status, stateDescription):Promise

```

### Create (a) archive(s) for a specific data subject access request
```
  /**
  * Transition the data subject access request processed state to create archive ready state. Your data collection
  * pipeline can perfom the following state transitions:
  *
  * PROCESSED_SUCCESS -> FILE_READY
  *
  * When an archive is created successfully, your user will be notified by the registered Email or phone number.
  *
  *
  * @param {String} darId a data subject access request ID
  * @param {String} cloudProvider one of the following values 'AWS' 'GOOGLE'
  * @param {String} bucketName a global bucket name usually in the naming convention of com-<company_name>-<bucket_name>
  *                          ATTENTION: Do NOT use . as part of bucket names in AWS S3. It will cause certificate issues.
  * @param {Array} fileNames A list of file names without any prefixes. They might look something like this:
  *                          [archive-001.zip, archive-002.zip, archive-003.zip]
  * 
  * @param {String} email user's email used to notify a user that the download is ready. It is your responsibility 
  *                       to pass in a valid email address. One of the email or phoneNumber param has to be present
  * @param {String} phoneNumber user's phone number used to notify a user that the download is ready. It is your
  *                             responsibility to pass in a valid phone number. One of the email or phoneNumber param 
  *                             has to be present
  * @returns {Promise}
  * @throws error can be cought in promise .catch chain. Use error.response.status to check the error code
  *         Error can be cought in try / catch block
  */
  createArchive(
      darId, 
      cloudProvider,
      bucketName, 
      fileNames,
      email, 
      phoneNumber):Promise
```
