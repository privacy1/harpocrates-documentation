# C# SDK Keychain

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization

#### Accessing a private Harpocrates instance
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host: keychain service name in kubernetes or IP address in dockers
* Keychain port: `8090`
* Service key: a base64 encoded service key that is used to derive the per user per service crypto key 16 bytes in size e.g `1E25F770B526D6727FB5B100F6CFB2AA`
* Cache expiration time which specifies how long time in seconds an encryption key is cached locally in the SDK instance
* Cerberus host: cerberus service name in kubernetes or IP address in dockers
* Cerberus port: `8093`
* A service account: an account used to connect to Keychain service. Provision it from Harpocrates Privacy Console
* A service account password used for authentication
* SDK cache size: (Optional) the cache size used by the SDK in Megabytes. If you don't set this value if will be default to 5 Megabytes
* SDK cache entry expiration: (Optional) every cache entry's expiration in seconds. If you don't set this value if will be default to 5 minutes

#### Accessing the SAAS Harpocrates instance
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host: `cloud.privacyone.co`
* Keychain port: `443`
* Service key: a base64 encoded service key that is used to derive the per user per service crypto key 16 bytes in size e.g `1E25F770B526D6727FB5B100F6CFB2AA`
* Cache expiration time: which specifies how long time in seconds an encryption key is cached locally in the SDK instance
* Cerberus host: `cloud.privacyone.co`
* Cerberus port: `443`
* A service account: an account used to connect to Keychain service. Provision it from Harpocrates Privacy Console
* A service account password used for authentication
* SDK cache size: (Optional) the cache size used by the SDK in Megabytes. If you don't set this value if will be default to 5 Megabytes
* SDK cache entry expiration: (Optional) every cache entry's expiration in seconds. If you don't set this value if will be default to 5 minutes

Every service communicating with the Keychain service should initialize a SDK instance. An authenticate function must be 
called to get a valid session token before using the SDK instance. The following example assuming we are using a SAAS verion
and https for communication. 

```
Keychain.Keychain keychain = new KeychainBuilder()
                .SetVersion("v2")
                .SetSchema("https")
                .SetKeychainHost("cloud.privacyone.co")
                .SetKeychainPort(443)
                .SetAuthHost("cloud.privacyone.co")
                .SetAuthPort(443)
                .SetServiceAccount("<YOUR_SERVICE_ACCOUNT>")
                .SetServicePassword("<YOUR_SERVICE_PASSWORD_FOR_AUTHENTICATION>")
                .SetServieKey("<YOUR_SERVICE_KEY_FOR_ENCRYPTION>")
                .SetCacheExpirationInSeconds(<SDK_CACHE>)               // optional if not set default to 5 MB
                .SetCacheExpireInSeconds(<SDK_CACHE_ENTRY_EXPIRATION>)  // optional if not set default to 5 Min
                .Build();

Keychain.KeychainPseudonymizer pseudonymizer;
pseudonymizer = new KeychainPseudonymizer(this.keychain);

```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  /**
   * Authenticate before access to keychain service
   * @return A session token that is valid for one hour will be retained by the SDK for future API communication. A re-authentication 
   *         is carried out automatically once the token has expired.
   * @throws RemoteServiceException
   */
  void authenticate() 
```

## User operations
You shall hookup Keychain API with your user registration flow so that Keychain service will be notified for each new 
user registration as well as existing user deletion. A User ID is the only information you need to pass down into Keychain 
service.

### Create user

```
  /**
   * Create a user with userId into keychain system. Invoke this API within your
   * new user registration process.
   *
   * @param userId Identifier of user.
   * @throws RemoteServiceException
   */
  void CreateUser(string userId)
```

### Delete user

```
  /**
   * Delete user from Keychain service.
   *
   * Soft or hard delete user by its user ID. Always execute a soft delete first. This
   * API serves as an archive functionality. After soft delete, subsequent API calls won't
   * be able to retrieve the user data. After a cool-off period you then execute
   * hard delete programatically or manually from Backstage software. A hard delete will
   * purge a user's data from the keychain database.
   *
   * @param userId Identifier of user.
   * @param hardDelete true to hard delete. false to soft delete.
   * @throws RemoteServiceException
   */
  public void DeleteUser(String userId, bool hardDelete)
```


## Data controls
There are two layer of controls. Primary given to data subjects that include consent and restriction. Secondary given to
data controller that include suspension and service termination. Primary controls are the rights granted to data subjects 
only and can be exercised from Harpocrates client software - Privacy Manager. Secondary controls can be executed by 
business.

A consent represents the ultimate permission a user has given your business for personal data processing if your legal basis 
is consent based.

A restriction represents a restriction or objection for your business to keep processing certain category of personal data. 
It can happen from legal proceedings, or simply users's privacy awareness increase.

### Read user controls
```
  /**
   * Get action, consent grant or revoke for user data
   * @param userId Identifier of user which is unique in Keychain service
   * @return UserDataControlModel
   * @throws RemoteServiceException
   */
  UserDataControlModel GetUserDataControls(String userId)
```

### Suspend data

```
  /**
   * Get action, consent grant or revoke for user data
   * @param userId the user identifier of the data subject
   * @param categoryId the data category ID
   * @return 
   * @throws RemoteServiceException
   */     
  void SetUserSuspension(string userId, int categoryId, bool suspended)
```


## Client-side pseudonymization operations
Cryptographic execution inside the SDK. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server 
side pseudonymization operation tradeoffs.


### Encrypt string
```
  /**
   * Encrypt string plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText bytes data needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] Encrypt(string userId, string category, string plainText)
```

### Decrypt to string
```
  /**
   * Decrypt cipher string to plaintext string
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized plaintext string
   * @throws EncryptionException
   */
  String DecryptToString(string userId, string category, byte[] ciphertext)
```

### Encrypt Datetime
```
  /**
   * Encrypt date time object text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText DateTime object needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] pseudonymizer.Encrypt(string userId, string category, DateTime plainText)
```

### Decrypt to Datetime
```
  /**
   * Decrypt cipher date time to plaintext object
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized plaintext DateTime object
   * @throws EncryptionException
   */
  DateTime DecryptToDateTime(string userId, string category, byte[] ciphertext) 
```

### Follow the same pattern, here are the list of APIs in the pseudonymizer

```
  public byte[] Encrypt (string userId, string category, byte[] plainText);
  public byte[] Encrypt (string userId, string category, bool plainText);
  public byte[] Encrypt (string userId, string category, char plainText);
  public byte[] Encrypt (string userId, string category, short plainText);
  public byte[] Encrypt (string userId, string category, ushort plainText);
  public byte[] Encrypt (string userId, string category, int plainText);
  public byte[] Encrypt (string userId, string category, uint plainText);
  public byte[] Encrypt (string userId, string category, long plainText);
  public byte[] Encrypt (string userId, string category, ulong plainText);
  public byte[] Encrypt (string userId, string category, float plainText);
  public byte[] Encrypt (string userId, string category, double plainText);
  public byte[] Encrypt (string userId, string category, string plainText);
  public byte[] Encrypt (string userId, string category, DateTime plainText);
  
  public byte[] DecryptToBytes (string userId, string category, byte[] ciphertext);
  public bool DecryptToBool (string userId, string category, byte[] ciphertext);
  public char DecryptToChar (string userId, string category, byte[] ciphertext);
  public short DecryptToShort (string userId, string category, byte[] ciphertext);
  public ushort DecryptToUnsignedShort (string userId, string category, byte[] ciphertext);
  public int DecryptToInt (string userId, string category, byte[] ciphertext);
  public uint DecryptToUnsignedInt (string userId, string category, byte[] ciphertext);
  public long DecryptToLong (string userId, string category, byte[] ciphertext);
  public ulong DecryptToUnsignedLong (string userId, string category, byte[] ciphertext);
  public float DecryptToUnsignedFloat (string userId, string category, byte[] ciphertext);
  public double DecryptToDouble (string userId, string category, byte[] ciphertext);
  public string DecryptToString (string userId, string category, byte[] ciphertext);
  public DateTime DecryptToDateTime (string userId, string category, byte[] ciphertext);
```
