# Java SDK LDAR

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

* LDAR host
* LDAR port default to 8091
* Cerberus host
* Cerberus port default to 8093
* Service name that is communicating with LDAR
* Service secret
      
Every service communicating with the LDAR service shall initialize a SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
/**
   * Constructor
   * @param ldarHost host address of ldar service
   * @param ldarPort port number of ldar service.
   * @param authHost host address of authentication service.
   * @param authPort port number of authentication service.
   * @param serviceName the name of service using this Ldar sdk. It is unique in the ldar backend service.
   * @param servicePassword the password for service in authentication service.
   */
   LdarClient(
      final String ldarHost,
      final int ldarPort,
      final String authHost,
      final int authPort,
      final String serviceName,
      final String servicePassword)
```


### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  /**
   * Authenticate before access to ldar service
   * 
   * @return Session token generated for the service which valid for a time period.
   * @throws RemoteServiceException
   */
   String authenticate()
```

## Data subject access requests operations
The following APIs are used to control the data subject access request processing flow. The workflow are divided into the
following phases.

* REQUESTED
* PROCESSING
* PROCESSED
* FILE_READY

Read `PhaseII/Subject Access Request` for more the design details.

### Read all requested data subject access requests (DARs)
Once a data subject access request has been registered through Privacy Manager, you backend API can start to track all 
pending requests from invoking this API. If you are processing access requests in batch pipeline, it is recommended to 
query them every day.

```
  /**
   * Get all requested data subject access requests. i.e. all pending requests
   * 
   * @return all requested data subject access requests
   * @throws RemoteServiceException
   */
   List<DarResponseModel> getRequestedDars() throws RemoteServiceException
```

### Read a single data subject access request
```
  /**
   * Get a data subject access request by its id
   * 
   * @param darId a data subject access request ID
   * @return all requested data subject access requests
   * @throws RemoteServiceException
   */
   DarResponseModel getDataAccessRequestByDarId(final String darId) throws RemoteServiceException
```

### Transition the workflow to PROCESSING state
```
  /**
   * Transition the data subject access request state into the processing state. Your data collection pipeline
   * can perform state transition as the following:
   *
   * REQUESTED -> PROCESSING
   * 
   * @param darProcessingModel Data Access Request Processing Model
   * @throws RemoteServiceException
   */
  void processingDar(darId, stateDescription):Promise
```

### Transition the workflow to PROCESSED state
```
  /**
   * Transition the data subject access request state into the processed state. Your data collection pipeline
   * can perfom the following state transitions:
   *
   * PROCESSING -> PROCESSED_SUCCESS
   * PROCESSING -> PROCESSED_FAILURE
   * PROCESSED_FAILURE -> PROCESSED_SUCCESS
   *  
   * @param darProcessedModel Data Access Request Processed Model
   * @throws RemoteServiceException
   */
   void processedDataAccessRequest(final DarProcessedModel darProcessedModel) throws RemoteServiceException 
```

### Create (a) archive(s) for a specific data subject access request
```
  /**
   * Transition the data subject access request processed state to create archive ready state. Your data collection
   * pipeline can perfom the following state transitions:
   *
   * PROCESSED_SUCCESS -> FILE_READY
   *
   * A CreateArchiveRequestModel object has the following properties
   * A String darId a data subject access request ID
   * An ArchiveStorageModel object with the following properties
   * public class ArchiveStorageModel {
   *   private String cloudProvider;    // one of the following values 'AWS' 'GOOGLE'
   *   private String bucketName;       // a global bucket name usually in the naming convention of com-<company_name>-<bucket_name>
   *                                    // ATTENTION: Do NOT use . as part of bucket names in AWS S3. It will cause certificate issues.
   *   private List<String> fileNames;  //  A list of file names without any prefixes. They might look something like this: [archive-001.zip, archive-002.zip, archive-003.zip]
   * }
   *
   * A UserContact object with the following properties
   * public class UserContact {
   *   private String email; // user's email used to notify a user that the download is ready. It is your responsibility to pass in a valid email address. One of the email or phoneNumber param has to be present
   *   private String phoneNumber; //user's phone number used to notify a user that the download is ready. It is your responsibility to pass in a valid phone number. One of the email or phoneNumber param has to be present
   * }
   *
   *
   * When an archive is created successfully, your user will be notified by the registered Email or phone number.
   *
   * @param createArchiveRequestModel instance of CreateArchiveRequestModel
   * @throws RemoteServiceException
   */
  public void createArchive(final CreateArchiveRequestModel createArchiveRequestModel) throws RemoteServiceException
```
