# Java SDK Keychain

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

#### Accessing a private Harpocrates instance
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host: keychain service name in kubernetes or IP address in dockers
* Keychain port: `8090`
* Service key: a base64 encoded service key that is used to derive the per user per service crypto key 16 bytes in size e.g `1E25F770B526D6727FB5B100F6CFB2AA`
* Cache expiration time which specifies how long time in seconds an encryption key is cached locally in the SDK instance
* Cerberus host: cerberus service name in kubernetes or IP address in dockers
* Cerberus port: `8093`
* A service account: an account used to connect to Keychain service. Provision it from Harpocrates Privacy Console.
* A service account password used for authentication
* If deterministic encryption needed. true if needed. Otherwise using non-deterministic encryption.

#### Accessing the SAAS Harpocrates instance
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host: `cloud.privacyone.co`
* Keychain port: `443`
* Service key: a base64 encoded service key that is used to derive the per user per service crypto key 16 bytes in size e.g `1E25F770B526D6727FB5B100F6CFB2AA`
* Cache expiration time: which specifies how long time in seconds an encryption key is cached locally in the SDK instance
* Cerberus host: `cloud.privacyone.co`
* Cerberus port: `443`
* A service account: an account used to connect to Keychain service. Provision it from Harpocrates Privacy Console.
* A service account password used for authentication
* If deterministic encryption needed. true if needed. Otherwise using non-deterministic encryption.

Every service communicating with the Keychain service should initialize an SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
  /**
   * Constructor
   * @param keychainHost host address of Keychain service
   * @param keychainPort port number of Keychain service.
   * @param authHost host address of authentication service.
   * @param authPort port number of authentication service.
   * @param serviceName the name of service using this Keychain sdk. It is unique in the Keychain backend service.
   * @param servicePassword the password for service in authentication service.
   * @param serviceKey key assigned by Keychain service which is used for authentication
   * @param keyCacheSize size of cache which store the consent encryption key for user.
   * @param keyCacheExpireInSeconds expire time of encryption key in cache. Unit is scondes.
   * @param isDeterministic If deterministic encryption needed. True if needed. otherwise using non-deterministic encryption
   */
   KeychainClient(
      final String keychainHost,
      final int keychainPort,
      final String authHost,
      final int authPort,
      final String serviceName,
      final String servicePassword,
      final String serviceKey,
      final long keyCacheSize,
      final long keyCacheExpireInSeconds,
      final boolean isDeterministic)
```
### Create keychain client
```
    keychainClient client = KeychainSdk.builder()
        .schema("http")
        .v2()
        .authHost(authHost)
        .authPort(8093)
        .keychainHost(keychainHost)
        .keychainPort(8090)
        .serviceName(serviceName)
        .servicePassword(servicePassword)
        .serviceKey(serviceKey)
        .cacheExpireInSeconds(120)
        .isDeterministic(true)
        .build();
```

You can also create a AsyncKeychainClien which has asynchronized API
```
    keychainClient client = KeychainSdk.builder()
        .schema("http")
        .v2()
        .authHost(authHost)
        .authPort(8093)
        .keychainHost(keychainHost)
        .keychainPort(8090)
        .serviceName(serviceName)
        .servicePassword(servicePassword)
        .serviceKey(serviceKey)
        .cacheExpireInSeconds(120)
        .isDeterministic(true)
        .build();

    # Create asyncKeychainClient with threadPoolSize = 10
    AsyncKeychainClient asyncClient = new AsyncKeychainClient(client, 10)
```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  /**
   * Authenticate before access to keychain service
   * @return Session token generated for the service which valid for a time period.
   * @throws RemoteServiceException
   */
  AuthResponseModel authenticate() 
```

## User operations
You shall hookup Keychain API with your user registration flow so that Keychain service will be notified for each new 
user registration as well as existing user deletion. A User ID is the only information you need to pass down into Keychain 
service.

### Create user

```
  /**
   * Create a user with userId into keychain system. Invoke this API within your
   * new user registration process.
   *
   * @param userId Identifier of user.
   * @throws RemoteServiceException
   */
  void createUser(final String userId) throws RemoteServiceException

    /**
   * Batch create users
   * @param userIds set of user id.
   * @throws RemoteServiceException
   */
  void createUsers(final Set<String> userIds) throws RemoteServiceException

```

### Delete user

```
  /**
   * Delete user from Keychain service.
   *
   * Soft or hard delete user by its user ID. Always execute a soft delete first. This
   * API serves as an archive functionality. After soft delete, subsequent API calls won't
   * be able to retrieve the user data. After a cool-off period you then execute
   * hard delete programatically or manually from Backstage software. A hard delete will
   * purge a user's data from the keychain database.
   *
   * @param userId Identifier of user.
   * @param hardDelete true to hard delete. false to soft delete.
   * @throws RemoteServiceException
   */
  public void deleteUser(final String userId, final boolean hardDelete)
```

## Get User Data Control
User Data Control information contains consents the the ultimate permission a user has given your business for personal data processing.


### Read user data control
```
  /**
   * Get user data controls in keychain service
   * @param userId
   * @return UserDataControlModel
   * @throws RemoteServiceException
   */
public UserDataControlModel getUserDataControl(final String userId) throws RemoteServiceException {

public class UserDataControlModel {
  private String userId;
  private Set<DataControlModel> dataControls;
}

public class DataControlModel {
  private Integer categoryId;
  private String categoryTitle;

  private Boolean granted;
  private Boolean restricted;
  private Boolean suspended;
}

```

### Suspend user data control
Service can suspend user's data control per request on authority demand.

```
  /**
   * Suspend user data control
   * @param userId
   * @param categoryId category on which suspended
   * @param suspended true to suspend. false to unsuspended.
   * @return
   * @throws RemoteServiceException
   */
  public UserDataControlModel setUserSuspension(final String userId,
                                               final Integer categoryId,
                                               final boolean suspended) throws RemoteServiceException {

```

## Client-side crypto operations
Cryptographic execution inside the SDK. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server 
side pseudonymization operation tradeoffs.

Use the Keychain client instance you have constructed from the above steps to get the encryptor for cryptographic operations.
If isDeterministic parameter is set true when construct Keychain client, the returned encryptor 
encrypt/decrypt data using deterministic encryption algorithm.
```
keychainClient.getEncryptor()
```

### Encrypt binary
```
  /**
   * Encrypt byte array plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText bytes data needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, byte[] plainText) throws EncryptionException
```

### Decrypt to binary
```
  /**
   * Decrypt byte array
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized bytes array
   * @throws EncryptionException
   */
  byte[] decryptToBytes(final String userId, final String category, byte[] encryptedText) throws EncryptionException 
```

### Encrypt a string
```
  /**
   * Encrypt string plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText String object that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, String plainString) throws EncryptionException 
```

### Decrypt to a string
```
  /**
   * Decrypt to string
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized String object
   * @throws EncryptionException
   */
  String decryptToString(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt int
```
  /**
    * Encrypt integer plain text
    * @param userId identifier of a user
    * @param category data category that the plaintext belongs to
    * @param plainText int value that needs to be pseudonymized
    * @return encrypted byte array
    * @throws EncryptionException
    */
  byte[] encrypt(final String userId, final String category, int plainText) throws EncryptionException 
```

### Decrypt to int
```
  /**
   * decrypt to int
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized int value
   * @throws EncryptionException
   */
  int decryptToInt(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt short
```
  /**
   * Encrypt short plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText short value that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, short plainText) throws EncryptionException 
```

### Decrypt to short
```
  /**
   * decrypt to short
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized short value
   * @throws EncryptionException
   */
  short decryptToShort(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt long
```
  /**
   * Encrypt long plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText long value that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, long plainText) throws EncryptionException
```

### Decrypt to long
```
  /**
   * decrypt to long
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized long value
   * @throws EncryptionException
   */
  long decryptToLong(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt float
```
 /**
  * Call keychain service endpoint to encrypt float plain text
  * @param userId identifier of a user
  * @param category data category that the plaintext belongs to
  * @param plainText float value that needs to be pseudonymized
  * @return encrypted byte array
  * @throws EncryptionException
  */
  public byte[] encryptFromRemoteService(String userId, String category, float plainText) throws EncryptionException
```

### Decrypt to float
```
  /**
   * decrypt to float
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized float value
   * @throws EncryptionException
   */
   float decryptToFloat(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt double
```
 /**
  * Call keychain service endpoint to encrypt double plain text
  * @param userId identifier of a user
  * @param category data category that the plaintext belongs to
  * @param plainText double value that needs to be pseudonymized
  * @return encrypted byte array
  * @throws EncryptionException
  */
  public byte[] encryptFromRemoteService(String userId, String category, double plainText) throws EncryptionException
```

### Decrypt to double
```
  /**
   * decrypt to double
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized double value
   * @throws EncryptionException
   */
  double decryptToDouble(final String userId, final String category, byte[] encryptedText) throws EncryptionException
```

### Encrypt a boolean
```
  /**
   * Encrypt boolean plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText boolean value that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
   byte[] encrypt(final String userId, final String category, boolean plainText) throws EncryptionException
```

### Decrypt to boolean
```
  /**
   * decrypt to boolean
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized boolean value
   * @throws EncryptionException
   */
  boolean decryptToBoolean(final String userId, final String category, byte[] encryptedText) throws EncryptionException 
```

### Encrypt a date object

```
  /**
   * Encrypt ZonedDateTime plain text. The precision is milliseconds.
   * Time zone is UTC. The data time object will be truncated to millisecond precision.  
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText ZonedDateTime object that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, ZonedDateTime plainText) throws EncryptionException 
```

### Decrypt to a date object
```
  /**
   * decrypt to a ZonedDateTime object
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @return de-pseudonymized ZonedDateTime object
   * @throws EncryptionException
   */
  ZonedDateTime decryptToDateTime(final String userId, final String category, byte[] encryptedText) throws EncryptionException 
```

### Encrypt enum
```
  /**
   * Encrypt enum plain text
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param plainText enum object that needs to be pseudonymized
   * @return encrypted byte array
   * @throws EncryptionException
   */
  byte[] encrypt(final String userId, final String category, Enum plainText) throws EncryptionException
```

### Decrypt to a enum
```
  /**
   * decrypt to enum
   * @param userId identifier of a user
   * @param category data category that the plaintext belongs to
   * @param encryptedText encrypted data that needs to be de-pseudonymized
   * @param clazz enum type of the plaintext data
   * @return de-pseudonymized enum object
   * @throws EncryptionException
   */
   <T extends Enum<T>> T decryptToEnum(
       final String userId, 
       final String category, 
       final byte[] encryptedText, 
       final Class<T> clazz) throws EncryptionException
```

## Server-side pseudonymization operations
Server side cryptographic execution. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server side
crypto operation tradeoffs.

### Encrypt a string
```
 /**
  * Encrpt plain text by calling keychain service encrypt endpoints from remote.
  * @param userId Identifier of user.
  * @param category consent category
  * @param plainText plain text need be encrypted.
  * @return encrypted byte arrays
  * @throws RemoteServiceException
  */
  byte[] encryptFromRemoteService(final String userId, final String category, final String plainText) throws RemoteServiceException 
```

### Decrypt a string
```
  /**
   * Decrypt text by calling keychain service encrypt endpoints from remote.
   * @param userId
   * @param category
   * @param encryptText
   * @return decrypted String
   * @throws RemoteServiceException
   */
   public String decryptToStringFromRemoteService(final String userId, final String category, final byte[] encryptText) throws RemoteServiceException 
```
## Format Preserved Encryption(FF3) tokenizer
A format Preserved Encryption tokenizer is implemented which can support personal number tokenization
and UUID tokenization. It supports tokenization for digits(0123456789) and digits-with-lowercase-characters
(0123456789abcdefghijklmnopqrstuvwxyz) and digits + characters(a-z, A-Z) which used for personal number and uuid.
```
  /**
   * Initialize a tokenizer.
   * @param key hex string key which length must be 32, 48, 64 representing 16, 24, 32 bytes. 
   *  e.g '83CF01EEF39E11ECB64D277EA6CC1A8C' reprsents 16 bytes key.
   * @param tweak 16 bytes String
   * @param type tokenizer for digits(0123456789) and digits with lowercase
   *             letters(0123456789 + abcdefghijklmnopqrstuvwxzy). etc.
   */
  public KeychainTokenizer(
      final String key,
      final String tweak,
      final TokenizerType type)
```

### Create FF3 tokenizer
#### Create tokenizer for digits
```
  /**
  * key is hex string which length is 32, 48 or 64.
  * tweek is hex string which length is 16
  */
  KeychainTokenizer tokenizer = KeychainSdk.builder().buildDigitsTokenizer(key, tweak);
```
#### Create tokenizer for digits with lowercase character.
```
  /**
  * key is hex string which length is 32, 48 or 64.
  * tweek is hex string which length is 16
  */
  KeychainTokenizer tokenizer = KeychainSdk.builder().buildDigitsLowerCharTokenizer(key, tweak);
```

#### Create tokenizer for digits with lowercase + uppercase character.
```
  /**
  * key is hex string which length is 32, 48 or 64.
  * tweek is hex string which length is 16
  */
  KeychainTokenizer tokenizer = KeychainSdk.builder().buildDigitsCharTokenizer(key, tweak);
```

### FF3 tokenizer API
#### Tokenize plain text
Encrypt the plain text string. The result is encrypted string which keep the format as before.
```
  String plainToken = "880921-3647";
  String cipherToken = tokenizer.encrypt(plainToken);
  assertTrue(cipherToken.equals("061999-3716");
  
  String plainToken = "5a347a6e-fe3c-11ec-857e-23e9e8e48e08";
  String cipherToken = tokenizer.encrypt(plainToken);
  assertTrue(cipherToken.equals("bixc92vf-oic4-qrqd-azei-m4p0bdo5mnsd");  
```
#### Detokenize cipher text
Decrypt the cipher text which is encrypted by tokenizer
```
  String cipherToken = "061999-3716";
  String plainText = tokenizer.decrypt(cipherToken);
  assertTrue(plainText.equals("880921-3647");
  
  String cipherToken = "bixc92vf-oic4-qrqd-azei-m4p0bdo5mnsd";
  String plainToken = tokenizer.encrypt(cipherToken);
  assertTrue(plainToken.equals("5a347a6e-fe3c-11ec-857e-23e9e8e48e08");  
```

