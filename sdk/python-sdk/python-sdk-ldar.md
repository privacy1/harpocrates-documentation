# Python SDK LDAR

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

* LDAR host
* LDAR port default 8091
* Cerberus host
* Cerberus port default 8093
* A service account that is going to connect to LDAR service
* A service account password

Every service communicating with the LDAR service shall initialize a SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
def __init__(self, ldarHost, ldarPort, cerberusHost, cerberusPort, accountName, accountPassword):
        """
        Constructor.
        :param ldarHost: ldar host address
        :param ldarPort: ldar host port
        :param cerberusHost: authentication host address
        :param cerberusPort: authentication host port
        :param accountName: the registered backend micro-service account name
        :param accountPassword: the registered backend micro-service account password
        """
```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  def authenticate(self):
      """
      First you need to register a service account into Cerberus system for your backend micro-service
      that needs access to keychain for personal data pseudonymisation. If you have multiple services
      that need access to keychain, it is recommended to register multiple accounts for keychain access.
      This method should be called before any other APIs.
      
      :return: None
      :exception RestRequestException
      """
```

## Data subject access requests operations
The following APIs are used to control the data subject access request processing flow. The workflow are divided into the
following phases.

* REQUESTED
* PROCESSING
* PROCESSED
* FILE_READY

Read `PhaseII/Subject Access Request` for more the design details.

### Read all requested data subject access requests (DARs)
Once a data subject access request has been registered through Privacy Manager, you backend API can start to track all 
pending requests from invoking this API. If you are processing access requests in batch pipeline, it is recommended to 
query them every day.

```
  def getRequestedDars(self):
      """
      Get all requested data subject access requests
      :return: Json object contains all requested dars.
      [
        {
          darId: 'efefefe111',
          statusTitle: 'REQUESTED',
          stateDescription: 'description about this dar',
          "products": [
            {
              "id": 1,
              "title": "email",
              "description": "all your email in plaintext",
              "iconLink": icon link
            },
            ...
          ],
          "archive": {
            "archiveId": 5,
            "archiveSize": "1GB",
            "archiveType": ".zip",
            "archiveExpirationDate": "1970-01-08T00:00:00Z"
          },
          "darCreationTime": "2020-01-08T00:00:00Z",
          "darLastUpdateTime": "1970-01-08T00:00:00Z"
        },
        {
           ...
        }
      ]
      """
```

### Read a single data subject access request (dar)
```
  def getDarById(self, darId):
      """
      Get a data subject access request by its dar id
      :param darId:
      :return: None
      {
        statusTitle: 'REQUESTED',
        stateDescription: 'description about this dar',
        "products": [
          {
             "productId": 1,
             "productTitle": "email",
             "productDescription": "all your email in plaintext"
          },
          ...
        ],
        "archive": {
        "archiveId": 5,
        "archiveSize": "1GB",
        "archiveType": ".zip",
        "archiveExpirationDate": "1970-01-08T00:00:00Z"
        }
      }
      """
```

### Transition the workflow to PROCESSING state
```
   def processingDar(self, darId, stateDescription):
       """
       Transition the data subject access request state into the processing state. Your data collection pipeline
       can perform state transition as the following:
       
       REQUESTED -> PROCESSING
       
       :param darId: darId a data subject access request ID
       :param stateDescription: stateDescription a description about the new state that is transitioned into
       :return: None
       """
```

### Transition the workflow to PROCESSED state
```
  def processedDar(self, darId, status, stateDescription):
       """
       Transition the data subject access request state into the processed state. Your data collection pipeline
       can perfom the following state transitions:

       PROCESSING -> PROCESSED_SUCCESS
       PROCESSING -> PROCESSED_FAILURE
       PROCESSED_FAILURE -> PROCESSED_SUCCESS
       
       :param darId: darId a data subject access request ID
       :param status a status can be either PROCESSED_SUCCESS or PROCESSED_FAILURE depending on the real data collection pipeline results
       :param stateDescription: stateDescription a description about the new state that is transitioned into

       :return: None
       """
```

### Create (a) archive(s) for a specific data subject access request
```
  def createArchive(self, darId, archiveStorage, email=None, phoneNumber=None):
      """
      Transition the data subject access request processed state to create archive ready state. Your data collection
      pipeline can perfom the following state transitions:
  
      PROCESSED_SUCCESS -> FILE_READY
  
      When an archive is created successfully, your user will be notified by the registered Email or phone number.

      :param darId: id of data subject access request
      :param archiveStorage: Json object. format is
      {
        "cloudProvider": "AWS/GOOGLE",
        "bucketName":    "bucketName a global bucket name usually in the naming convention of com-<company_name>-<bucket_name>",
                          "ATTENTION: Do NOT use . as part of bucket names in AWS S3. It will cause certificate issues."

        "fileNames": [
           "archive-001.zip",
           "archive-002.zip",
           ...
        ]
      }
      :param email: email user's email used to notify a user that the download is ready. It is your responsibility 
                    to pass in a valid email address. One of the email or phoneNumber param has to be present
      :param phoneNumber: user's phone number used to notify a user that the download is ready. It is your
                          responsibility to pass in a valid phone number. One of the email or phoneNumber param has to be present
      :return: None
      """
```
