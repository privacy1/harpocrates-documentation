# Python SDK Keychain

## SDK setup
You shall construct an SDK instance and perform authentication against the Cerberus service before invoking any SDK methods.

### SDK initialization
Configuration parameters that are needed for to construct an SDK instance including 

* Keychain host
* Keychain port default 8090
* Service key a service specific key that is used to derive the per user per service crypto key
* Cache expiration time which specifies how long time in seconds an encryption key is cached locally in the SDK instance
* Cerberus host
* Cerberus port default 8093
* A service account that is going to connect to Keychain service
* A service account password

Every service communicating with the Keychain service should initialize a SDK instance.
An authenticate function must be called to get a valid session token before using the SDK instance.

```
def __init__(self, keychainHost, keychainPort, authHost, authPort, accountName, accountPassword,
                 serviceKey, cacheSize=None, expireInSeconds=None):
        """
        Constructor.
        :param keychainHost: keychain host address
        :param keychainPort: keychain host port
        :param authHost: authentication host address
        :param authPort: authentication host port
        :param accountName: the registered backend micro-service account name
        :param accountPassword: the registered backend micro-service account password
        :param serviceKey: the service key for keychain key derivation
        :param cacheSize: consent key cache size
        :param expireInSeconds: consent key cache expiration in seconds
        """
```

### SDK Authentication
An authenticate function must be called to get a valid session token before using the SDK instance!

```
  def authenticate(self):
      """
      First you need to register a service account into Cerberus system for your backend micro-service
      that needs access to keychain for personal data pseudonymisation. If you have multiple services
      that need access to keychain, it is recommended to register multiple accounts for keychain access.
      This method should be called before any other APIs.
      :return: None
      :exception RestRequestException
      """
```

## User operations
You shall hookup Keychain API with your user registration flow so that Keychain service will be notified for each new 
user registration as well as existing user deletion. A User ID is the only information you need to pass down into Keychain 
service.

### Create user

```
  def createUser(self, userId):
      """
      Create a user with userId into keychain system. Invoke this API within your new user registration process.
      
      :param userId: string. a user identifier.
      :return: None
      """
```

### Delete user

```
  def deleteUser(self, userId, trueDelete=False):
      """
      Delete user from Keychain service.
   
      Soft or hard delete user by its user ID. Always execute a soft delete first. This
      API serves as an archive functionality. After soft delete, subsequent API calls won't
      be able to retrieve the user data. After a cool-off period you then execute
      hard delete programatically or manually from Backstage software. A hard delete will
      purge a user's data from the keychain database.

      :param userId: string. a user identifier.
      :trueDelete: false soft delete. true hard delete.
      :return: None
      """
```

## Consent operations
A consent represents the ultimate permission a user has given your business for personal data processing if your legal basis 
is consent based.

### Read consents

```
  def getConsent(self, userId):
      """
      Get the user consents.
      :param userId: string. a user identifier.
      :return: Json object contains all user consents. The format is
      {
        "userId": "string",
        "consents": [
          {
            "categoryTitle": "location",
            "granted": true
          },
          {
            "categoryTitle": "image",
            "granted": false
          }
        ],
      }
      """
```

### Write single consent
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

**NOTE: Withdrawing a consent = restrict future data processing + historical data deletion** 

```
  def setConsent(self, userId, categoryTitle, granted):
      """
      Set user consent on specified a data category.
      :param userId: string. a user identifier
      :param categoryTitle: string. a consent data category
      :param granted: boolean. True to grant the consent. False to revoke the consent
      :return: None
      :exception RestRequestException
      """
```

### Write multiple consents
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

**NOTE: Withdrawing a consent = restrict future data processing + historical data deletion** 

```
  def setConsents(self, consent):
      """
      Set user consent on specified data categories. Multiple data categories can be set at the same time.
      :param consent: JSON object. The format is
      {
        "userId": "string",
        "consents": [
          {
            "categoryTitle": "location",
            "granted": true
          },
          {
            "categoryTitle": "image",
            "granted": false
          }
        ],
      }
      :return: None
      :exception: RestRequestException if request failed.
      """
```

## Restriction operations
### Read restriction

```
  def getRestrictions(self, userId):
      """
      Get the user's restrictions on data that belong to specific categories
      :param userId: string. a user identifier
      :return: Json object contains user restriction of data that belong to specific categories. The format is
      {
        "userId": "string",
        "restrictions": [
          {
            "categoryTitle": "location",
            "restricted": true
          },
          {
            "categoryTitle": "image",
            "restricted": false
          }
        ]
      }
      :exception: RestRequestException
      """
```

### Write a single restriction
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

```
  def setRestriction(self, userId, categoryTitle, restricted):
      """
      Set user restriction on specified a data category. Multiple data categories can be set at the same time.
      :param userId: string. user Id
      :param categoryTitle: string. consent category
      :param restricted: boolean. True to restrict the consent. false otherwish
      :return: None
      :exception RestRequestException
      """
```

### Write multiple restrictions
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`

```
  def setRestrictions(self, restriction):
      """
      Set user restrictions on specified data categories. Multiple data categories can be set at the same time.
      :param restriction: Json object. The format is
      {
        "userId": "string",
        "restrictions": [
          {
            "categoryTitle": "location",
            "restricted": true
          },
          {
            "categoryTitle": "image",
            "restricted": false
          }
        ]
      }
      :return: None
      :exception: RestRequestException when request failed.
      """
```

## Deletion operations
**NOTE: deleting data is permanent**. This API will be triggered by a data subject from within Privacy Manager, 
should you choose to allow him/her carry out such an action. Otherwise, your backend services can trigger it periodically.

### Delete user personal data from multiple data categories
The keychain.consent.write resource/permission is needed for a service account to execute this API. Refer to document `Phase II/Data Permission`


```
  def setDeletions(self, deletions):
     """
      Delete user data that belong to specific categories permanently. 
      :param deletions: Json object. format is
      {
        "userId": "string",
        "deletionModel": [
          "location",
          "image"
        ]
      }
      :return: None
     """
```

## Client-side crypto operations
Cryptographic execution inside the SDK. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server 
side crypto operation tradeoffs.

### Encrypt bytes
```
  def encryptBytes(self, userId, category, plainTextBytes):
      """
      Encrypt byte arrays.
      :param userId: string. user id.
      :param category: string. consent category
      :param plainTextBytes: byte arrays to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to bytes
```
  def encryptBytes(self, userId, category, encryptedBytes):
      """
      Decrypt encrypted byte arrays
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext byte arrays
      """
```

### Encrypt a string
```
  def encryptString(self, userId, category, plainString):
      """
      Encrypt plaintext string
      :param userId: string. a user id
      :param category: string. a consent category
      :param plaintextString: plaintext string to be pseudomymized
      :return: encrypted byte arrays.
      """
```

### Decrypt to a string
```
  def decryptToString(self, userId, category, encryptedStringBytes):
      """
      decrypt encrypted byte arrays to string
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedStringByte: encrypted byte arrays to be de-pseudomymized
      :return: plaintext string.
      """
```

### Encrypt int
```
  def encryptInt(self, userId, category, integerValue):
      """
      Encrypt integer value to byte arrays
      :param userId: string. a user id
      :param category: string. a consent category
      :param integerValue: integer value to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to int
```
  def decryptToInt(self, userId, category, encryptedIntBytes):
  """
      decrypt encrypted byte arrays to integer value by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedIntBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext integer value.
      """
```

### Encrypt float
```
  def encryptFloat(self, userId, category, floatValue):
      """
      encrypt float value to byte arrays
      :param userId: string. a user id
      :param category: string. a consent category
      :param floatValue: float value to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to float
```
  def decryptToFloat(self, userId, category, encryptedFloatBytes):
      """
      decrypt encrypted byte arrays to float value
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedFloatBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext float value.
      """
```

### Encrypt a date time object
```
  def encrypteDateTime(self, userId, category, dateTime):
      """
      Encrypt datetime. Input data time must be UTC. precision is milliseconds.
      :param userId: string. a user id
      :param category: string. a consent category
      :param dateTime: datetime to be pseudomymized. must be UTC
      :return: encrypted byte arrays
      """
```

### Decrypt a date object
```
  def decryptToDateTime(self, userId, category, encryptedDateTimeBytes):
      """
      decrypt encrypted byte arrays to date time object
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedDateTimeBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext date time object.
      """
```

## Server-side crypto operations
Server side cryptographic execution. Read the `Phase-II/Data Pseudonimizatoin` to understand client side or server side
crypto operation tradeoffs.

### Encrypt bytes
```
  def encryptBytesFromService(self, userId, category, plainTextBytes):
      """
      Encrypt byte arrays by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param plainTextBytes: byte arrays to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to bytes
```
  def decryptBytesFromService(self, userId, category, encryptedBytes):
      """
      Decrypt encrypted byte arrays by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext byte arrays
      """
```

### Encrypt a string
```
  def encryptStringFromService(self, userId, category, plaintextString):
      """
      Encrypt plaintext string by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param plaintextString: plaintext string to be pseudomymized
      :return: encrypted byte arrays.
      """
```

### Decrypt to a string
```
  def decryptStringFromService(self, userId, category, encryptStringByte):
      """
      decrypt encrypted byte arrays to string by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptStringByte: encrypted byte arrays to be de-pseudomymized
      :return: plaintext string.
      """
```

### Encrypt int
```
  def encryptIntFromService(self, userId, category, integerValue):
      """
      encrypt integer value to string by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param integerValue: integer value to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to int
```
  def decryptIntFromService(self, userId, category, encryptedIntBytes):
      """
      decrypt encrypted byte arrays to integer value by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedIntBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext integer value.
      """
```

### Encrypt float

```
  def encryptFloatFromService(self, userId, category, floatValue):
      """
      encrypt float value to byte arrays by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param floatValue: float value to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to float
```
  def decryptFloatFromService(self, userId, category, encryptedFloatBytes):
      """
      decrypt encrypted byte arrays to float value by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedFloatBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext float value.
      """
```

### Encrypt date time
```
  def encryptDateTimeFromService(self, userId, category, dateTime):
      """
      encrypt data time object to byte arrays by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param dateTime: data time object to be pseudomymized
      :return: encrypted byte arrays
      """
```

### Decrypt to date time

```
  def decryptDateTimeFromService(self, userId, category, encryptedDateTimeBytes):
      """
      decrypt encrypted byte arrays to date time object by calling backend keychain service
      :param userId: string. a user id
      :param category: string. a consent category
      :param encryptedDateTimeBytes: encrypted byte arrays to be de-pseudomymized
      :return: plaintext date time object.
      """
```
