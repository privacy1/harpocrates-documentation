# Overview
Keychain and LDAR service provide RESTful APIs that can be invoked by your backend system. To make the API calls more 
friendly, PRIVACY1 provides native SDKs to support multiple programming languages, which include Java, Python and Node.js.
If you want another programming language support, please register your request [here](https://privacyone.co/contact).

# Prepare Keychain system
Before you approach Keychain SDK and start pseudonymize personal data. Please read the Golden Path Document and understand
our architecture. Then follow the steps
1. Go to Privacy Console, Data Permission module to create a data domain
2. Go to Privacy Console, Data permission module to create a new service account
3. Go to Privacy Console, Data Control & Security module to create data category and its localizations. Be aware when you
   create a specific data category, a default `primary` data category will be created on behalf of you. This category is
   primarily used for essentail data that a user can not opt out in your business context. Such as login Email.
 
# SDK in your favorite languages
## Java SDK 
View [Java SDK](https://mvnrepository.com/artifact/co.privacyone) dependencies from maven central repo.

### Add Keychain maven dependency
```
<dependency>
  <groupId>co.privacyone.sdk</groupId>
  <artifactId>keychain</artifactId>
  <version>2.1.0</version>
</dependency>
```

### Add LDAR maven dependency
```
<dependency>
    <groupId>co.privacyone.sdk</groupId>
    <artifactId>ldar</artifactId>
    <version>2.1.0</version>
</dependency>
```

## Node.js SDK 
View and download [Node.js SDK](https://www.npmjs.com/package/p1-harpocrates-sdk) from npm registry.
To install Js sdk simply run
```
$ npm i p1-harpocrates-sdk
```

## Python SDK 
View and download [Python SDK](https://pypi.org/project/p1-harpocrates-sdk) from python repository.

To install Python sdk simply run
```
$ pip install p1-harpocrates-sdk
```

## C# SDK 
View and download [C# SDK](https://www.nuget.org/packages/Privacy1.Harpocrates/) from nuget repository.

To install c# sdk, go to your visual studio -> choose Dependencies -> Search for Harpocrates and download 
the dependency or run the following command

```
paket add Privacy1.Harpocrates --version 1.0.3
```

